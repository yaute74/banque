#include "src/MainUI.h"
#include "src/scateg/SCategCbDelegate.h"
#include "src/tools/QBoxCalendar.h"
#include "src/prev/add/PrevAdd.h"
#include "src/prev/manage/PrevManage.h"
#include "src/scateg/CategManage.h"
#include "src/autochk/QDialogReleve.h"
#include "src/graphic/RepartitionCateg.h"
#include "src/graphic/Histographe.h"
#include <set>


MainUI::MainUI(const QString banque, int minYear) : m_refreshAfterSave(true), m_minYear(minYear)
{
	this->setGeometry(0, 0, 1250, 800);
	this->setWindowTitle(banque);
	//this->setWindowIcon(QIcon(":/images/logo.png"));

	// Center frame
	QRect r = this->geometry();
	r.moveCenter(QApplication::desktop()->availableGeometry().center());
	this->setGeometry(r);

	m_idxListLastChk = false;
	m_table = new MouvementsTable(this);
	createMenus();
	ceateCentralWidget();
}

void MainUI::createMenus()
{
	// Mouvements
	QMenu *menuMvt = menuBar()->addMenu(tr("&Gestion des mouvements"));
	QAction *actMvtAdd = new QAction(QIcon(":/images/add.png"), tr("Ajouter un mouvement"), this);
	QAction *actMvtSave = new QAction(QIcon(":/images/save.png"), tr("Enregistrer les modifications"), this);
	QAction *actMvtDel = new QAction(QIcon(":/images/del.png"), tr("Supprimer le mouvement"), this);
	menuMvt->addAction(actMvtAdd);
	actMvtAdd->setShortcut(QKeySequence("Ctrl+N"));
	menuMvt->addAction(actMvtSave);
	actMvtSave->setShortcut(QKeySequence("Ctrl+S"));
	menuMvt->addAction(actMvtDel);

	menuMvt->addSeparator();
	m_currentYear = new QSpinBox(this);

	m_currentYear->setMinimum(m_minYear);
	m_currentYear->setMaximum(QDate::currentDate().year()+1);
	m_currentYear->setValue(QDate::currentDate().year());

	menuMvt->addSeparator();
	menuMvt->addSeparator();
	QAction *actMvtCateg = new QAction(tr("Gestion des catégories"), this);
	menuMvt->addAction(actMvtCateg);

	menuMvt->addSeparator();
	QAction *actMvtReleve = new QAction(QIcon(":/images/releve.png"), tr("Extraire relevé de compte"), this);
	menuMvt->addAction(actMvtReleve);

	// Gestion des checks
	QMenu *menuChk = menuBar()->addMenu(tr("&Gestion des checks"));
	QAction *actChkValidate = new QAction(QIcon(":/images/check.png"), tr("Valider les checks"), this);
	QAction *actChkDisable = new QAction(tr("Annuler les checks"), this);
	menuChk->addAction(actChkValidate);
	menuChk->addAction(actChkDisable);

	// Gestion des prévisions
	QMenu *menuPrev = menuBar()->addMenu(tr("&Gestion des prévisions"));
	QAction *actPrevManage = new QAction(tr("Gestion des prévisions"), this);
	QAction *actPrevAdd = new QAction(QIcon(":/images/prev.png"), tr("Ajouter les prévisions du mois"), this);
	menuPrev->addAction(actPrevAdd);
	menuPrev->addAction(actPrevManage);
	m_chkSearchExact = new QCheckBox("Mot entier", this);
	m_searchLine = new QLineEdit(this);
	m_searchLine->setMaximumWidth(200);
	QAction *actSearchAfter = new QAction(QIcon(":/images/searchn.png"), tr("Suivant"), this);
	QToolBar *toolBarMain = addToolBar("Main");
	toolBarMain->setIconSize(QSize(32, 32));
	toolBarMain->addAction(actMvtAdd);
	toolBarMain->addAction(actMvtSave);
	toolBarMain->addAction(actMvtDel);
	toolBarMain->addSeparator();
	toolBarMain->addAction(actMvtReleve);
	toolBarMain->addSeparator();
	toolBarMain->addWidget(m_currentYear);
	toolBarMain->addSeparator();
	toolBarMain->addAction(actChkValidate);
	toolBarMain->addSeparator();
	toolBarMain->addAction(actPrevAdd);
	toolBarMain->addSeparator();
	toolBarMain->addWidget(m_searchLine);
	toolBarMain->addWidget(m_chkSearchExact);
	toolBarMain->addAction(actSearchAfter);

	// Gestion des prévisions
	QMenu *menuGraph = menuBar()->addMenu(tr("Graphique"));
	QAction *actGraphTest = new QAction(tr("Répartition par catégorie"), this);
	menuGraph->addAction(actGraphTest);

	connect(actMvtAdd, SIGNAL(triggered()), this, SLOT(addLine()));
	connect(actMvtDel, SIGNAL(triggered()), this, SLOT(delLine()));
	connect(actMvtSave, SIGNAL(triggered()), this, SLOT(saveLines()));
	connect(actPrevAdd, SIGNAL(triggered()), this, SLOT(addPrevision()));
	connect(actChkValidate, SIGNAL(triggered()), this, SLOT(validateCheck()));
	connect(actChkDisable, SIGNAL(triggered()), this, SLOT(disableCheck()));
	connect(actPrevManage, SIGNAL(triggered()), this, SLOT(managePrevision()));
	connect(actMvtCateg, SIGNAL(triggered()), this, SLOT(manageCategories()));
	connect(actSearchAfter, SIGNAL(triggered()), this, SLOT(searchTable()));
	connect(m_searchLine, SIGNAL(returnPressed()), this, SLOT(searchTable()));
	connect(m_currentYear, SIGNAL(valueChanged(int)), this, SLOT(changeDate(int)));
	connect(actMvtReleve, SIGNAL(triggered()), this, SLOT(extractReleve()));

	connect(actGraphTest, SIGNAL(triggered()), this, SLOT(testGraph()));

}


void MainUI::ceateCentralWidget()
{
	m_table->setItemDelegateForColumn(MouvementsModel::COL_CATEG, new SCategCbDelegate(this));
	m_table->setSelectionMode(QAbstractItemView::SingleSelection);
	m_table->resizeColumnsToContents();

	QFrame *mainFrame = new QFrame();
	QHBoxLayout *layout = new QHBoxLayout();
	QGridLayout *rightlayout = new QGridLayout();
	m_frameSumCheck = new FrameSumCheck(this);
	m_frameFilter = new FrameFilter(this);

	m_frameSumCheck->setMaximumWidth(250);
	m_frameFilter->setMaximumWidth(250);

	rightlayout->addWidget(m_frameSumCheck, 0, 0, 1, 1);
	rightlayout->addWidget(m_frameFilter, 1, 0, 10, 1);
	//rightlayout->setSizeConstraint(QLayout::SetMinimumSize);
	layout->addWidget(m_table);
	//layout->addWidget(m_frameSumCheck);
	layout->addLayout(rightlayout);

	mainFrame->setLayout(layout);
	setCentralWidget(mainFrame);

	setModel(m_currentYear->value());
}


void MainUI::setModel(int year)
{
	try
	{
		m_model = new MouvementsModel(this, m_frameFilter, year);
		m_table->setModel(m_model);
		m_table->resizeColumnsToContents();

		updSumCheck();

		//m_tableMvt->scrollToBottom();
		m_table->scrollTo(m_model->index(m_model->rowCount()-24, 0));
		connect(m_model, SIGNAL(changeSumCheck()), this, SLOT(updSumCheck()));

	}
	catch(QString const& chaine)
	{
		m_model = new MouvementsModel(this, NULL); // pas terrible ...
		m_table->setModel(m_model);
		m_table->resizeColumnsToContents();
		QMessageBox::critical(NULL, "Erreur SQL", chaine);
		//throw QString(chaine);
	}

}

void MainUI::updSumCheck()
{
	m_frameSumCheck->setCurCheck(m_model->getSumCheck());
	m_frameSumCheck->setCurFullCheck(m_model->getSumFullCheck());
}

void MainUI::delLine()
{
	QModelIndexList indexes = m_table->selectionModel()->selection().indexes();
	if (indexes.count() > 0)
	{
		m_model->delLine(indexes.at(0).row());
	}
}

void MainUI::addLine()
{
	QDate newDate = QBoxCalendar::getDate(this);
	if (newDate.isValid())
	{
		QModelIndex index = m_model->addLine(newDate);
		 m_table->selectionModel()->select(index, QItemSelectionModel::Select);
		 m_table->scrollTo(index);
	}
}

bool MainUI::saveLines()
{
	m_table->commitCloseEditor();
	if (m_model->saveLines())
	{
		if (m_refreshAfterSave)
		{
			delete m_model;
			setModel(m_currentYear->value());
		}
	}
	else
	{
		QMessageBox::critical(this, tr("Erreur"), m_model->getLastError());
		return false;
	}
	return true;
}

bool MainUI::confirmedSave(bool *doRefresh)
{
	m_table->commitCloseEditor();
	if (m_model->mustSave())
	{
		int reponse = QMessageBox::question(this, tr("Modifications"), tr("Sauvegarder les modifications ?"), QMessageBox ::Yes | QMessageBox::No | QMessageBox::Cancel);

		if (reponse == QMessageBox::Yes)
		{
			if (!saveLines())
			{
				return false;
			}
			if (doRefresh != Q_NULLPTR)
				*doRefresh = true;
		}
		else if (reponse == QMessageBox::Cancel)
		{
			return false;
		}
	}

	return true;
}


void MainUI::closeEvent(QCloseEvent *event)
{
	if (confirmedSave())
    {
		event->accept();
        exit(0);
    }
	else
		event->ignore();
}

void MainUI::addPrevision()
{
	std::set<qlonglong> prevInCache = m_model->getPrevInCache();
	PrevAdd prev(this, prevInCache);
	QModelIndex lastindex;
	int minRow = -1;

	if(prev.exec() == QDialog::Accepted)
	{
		std::vector<PrevAddObj*> m_data = prev.getPrevList();
		std::vector<PrevAddObj*>::iterator it;
		int year = prev.getYear();
		int month = prev.getMonth();

		for (it = m_data.begin(); it != m_data.end(); ++it)
		{
			QDate date(year, month, (*it)->getJour());
			lastindex = m_model->addLine(date, (*it)->getId(), (*it)->getDebit(), (*it)->getScategorie());
			if (minRow == -1 || lastindex.row() < minRow)
			{
				minRow = lastindex.row();
			}
		}

		if (minRow > -1)
		{
			m_model->recalculSum(minRow);
		}
		m_table->selectionModel()->select(lastindex, QItemSelectionModel::Select);
		m_table->scrollTo(lastindex);
	}
}

void MainUI::managePrevision()
{
	PrevManage *prev = new PrevManage(this);
	prev->setWindowModality(Qt::ApplicationModal);
	prev->show();
}

void MainUI::validateCheck()
{
	m_model->updCheck(true);
}

void MainUI::disableCheck()
{
	m_model->updCheck(false);
}

void MainUI::manageCategories()
{
	CategManage *categ = new CategManage(this);
	categ->setWindowModality(Qt::ApplicationModal);
	categ->show();
}

void MainUI::setFilter()
{
	m_refreshAfterSave = false;
	if (confirmedSave())
	{
		delete m_model;
		setModel(m_currentYear->value());
	}
	m_refreshAfterSave = true;
}

void MainUI::changeDate(int i)
{
	m_refreshAfterSave = false;
	if (confirmedSave())
	{
		delete m_model;
		setModel(i);
	}
	m_refreshAfterSave = true;
}

void MainUI::searchTable()
{
	if (m_searchLine->text().length() > 0)
	{
		if (m_searchLine->text() != m_lastsearch || m_idxListLastChk != m_chkSearchExact->isChecked())
		{
			m_lastsearch = m_searchLine->text();

			bool isNum =false;
			int col = MouvementsModel::COL_CATEG;
			m_lastsearch.toDouble(&isNum);
			if (isNum)
				col = MouvementsModel::COL_DEB;

			Qt::MatchFlags flags = Qt::MatchWrap;
			m_idxListLastChk = m_chkSearchExact->isChecked();
			if (m_idxListLastChk)
			{
				flags |= Qt::MatchExactly;
			}
			else
			{
				flags |= Qt::MatchContains;
			}

			m_idxListSearch.clear();
			m_idxListSearch = m_model->match(m_model->index(0, col), Qt::DisplayRole, m_lastsearch, -1, flags);
			m_idxListLastPos = -1;
		}

		if (m_idxListSearch.size() > 0)
		{
			m_idxListLastPos++;

			if (m_idxListLastPos >= m_idxListSearch.size())
			{
				m_idxListLastPos = 0;
			}
			m_table->selectionModel()->select(m_idxListSearch.at(m_idxListLastPos), QItemSelectionModel::Clear | QItemSelectionModel::Select);
			m_table->scrollTo(m_idxListSearch.at(m_idxListLastPos));
		}
	}
	else
	{
		m_lastsearch = "";
	}
}

void MainUI::extractReleve()
{
	// TODO demander a sauvegarder
	bool mustRefresh = false;
	m_refreshAfterSave = false;
	if (confirmedSave(&mustRefresh))
	{
		if (QDialogReleve::autoCheck(this) || mustRefresh)
		{
			delete m_model;
			setModel(m_currentYear->value());
		}
	}
	m_refreshAfterSave = true;
}

void MainUI::testGraph()
{
	RepartitionCateg *test = new RepartitionCateg();
	Histographe *graphe = new Histographe(test);
	graphe->show();
}

MainUI::~MainUI()
{
	delete m_model;
}

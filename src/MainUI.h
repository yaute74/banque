#ifndef MAINUI_H
#define MAINUI_H

#include "src/mvt/MouvementsModel.h"
#include "src/FrameSumCheck.h"
#include "src/FrameFilter.h"
#include "src/mvt/MouvementsTable.h"
#include <QMainWindow>
#include <QtWidgets>


class MainUI: public QMainWindow
{
	Q_OBJECT

public:
	MainUI(const QString banque, int minYear);
	~MainUI();

public slots:
	void delLine();
	void addLine();
	bool saveLines();
	void addPrevision();
	void validateCheck();
	void disableCheck();
	void updSumCheck();
	void managePrevision();
	void manageCategories();
	void setFilter();
	void searchTable();
	void changeDate(int i);
	void extractReleve();
	void testGraph();

protected:
	void closeEvent(QCloseEvent *event);

private:
	void createMenus();
	void ceateCentralWidget();
	void setModel(int year = -1);
	void loadMouvemenets();
	bool confirmedSave(bool *doRefresh = Q_NULLPTR);

	MouvementsModel *m_model;
	MouvementsTable *m_table;
	FrameSumCheck *m_frameSumCheck;
	FrameFilter *m_frameFilter;
	bool m_refreshAfterSave;
	QLineEdit *m_searchLine;
	QString m_lastsearch;
	QModelIndexList m_idxListSearch;
	int m_idxListLastPos;
	bool m_idxListLastChk;
	QCheckBox *m_chkSearchExact;
	QSpinBox *m_currentYear;
	int m_minYear;
};

#endif // MAINUI_H

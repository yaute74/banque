#include "QBoxCalendar.h"

QBoxCalendar::QBoxCalendar(QWidget *parent) : QDialog(parent)
{
    createLayout();
}

QBoxCalendar::QBoxCalendar(QWidget *parent, QDate defdate) : QDialog(parent), m_defdate(defdate)
{
    createLayout();
}

void QBoxCalendar::createLayout()
{
	QHBoxLayout *calendarlayout = new QHBoxLayout;
	m_wcalendrier = new QCalendarWidget(this);

    if (m_defdate.isValid())
    {
        m_wcalendrier->setSelectedDate(m_defdate);
    }
	calendarlayout->addWidget(m_wcalendrier);
	this->setLayout(calendarlayout);

	connect(m_wcalendrier, SIGNAL(clicked(const QDate)), this, SLOT(accept()));
}

QDate QBoxCalendar::getSelectDate()
{
	return m_wcalendrier->selectedDate();
}


QDate QBoxCalendar::getDate(QWidget *parent)
{
	QBoxCalendar * dlg = new QBoxCalendar(parent);
	QDate mydate;

	if (dlg->exec() == QDialog::Accepted)
	{
		mydate = dlg->getSelectDate();
	}

	delete dlg;
	return mydate;
}

QDate QBoxCalendar::getDate(QWidget *parent, QDate defdate)
{
    QBoxCalendar * dlg = new QBoxCalendar(parent, defdate);
    QDate mydate;

    if (dlg->exec() == QDialog::Accepted)
    {
        mydate = dlg->getSelectDate();
    }

    delete dlg;
    return mydate;
}

QBoxCalendar::~QBoxCalendar()
{
	delete m_wcalendrier;
}

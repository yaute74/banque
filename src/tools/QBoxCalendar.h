#ifndef QBOXCALENDAR_H
#define QBOXCALENDAR_H

#include <QtWidgets>

class QBoxCalendar : public QDialog
{

public:
	QBoxCalendar(QWidget *parent);
    QBoxCalendar(QWidget *parent, QDate defdate);
	~QBoxCalendar();

	static QDate getDate(QWidget *parent);
    static QDate getDate(QWidget *parent, QDate defdate);

private:
	QDate getSelectDate();
    void createLayout();
	QCalendarWidget * m_wcalendrier = new QCalendarWidget;
    QDate m_defdate;
};


#endif // QBOXCALENDAR_H

#ifndef CHOICEMVT_H
#define CHOICEMVT_H

#include <QtWidgets>
#include "src/scateg/QComboSCateg.h"

class ChoiceMvt : public QDialog
{
	Q_OBJECT

public:
	ChoiceMvt(QWidget *parent, QDate mvtDate, double value, QString intitule);
	static QDialogButtonBox::StandardButton getMvt(QWidget *parent, QDate mvtDate, double value, QString intitule);

protected:
	QDialogButtonBox::StandardButton getRetCode();
	qlonglong getMvtId();
	qlonglong getSCategId();
    double getValue();
	void closeEvent(QCloseEvent *event);

public slots:
	void buttons(QAbstractButton *button);
    //void plusMoins();


private:
	void createLayout();
	void mvtList(QButtonGroup *grpButtonMvt, QGridLayout *newMvtLayout);
	void searchSCateg();
	void setReveleSCateg(qlonglong  scategId);

	QDate m_mvtDate;
	double m_releveValue;
	QString m_releveCateg;
	QDialogButtonBox *m_bouton;
	QDialogButtonBox::StandardButton m_ret;
	QLabel *m_lblValue;
//	QRadioButton *m_rdPlus;
//	QRadioButton *m_rdMoins;
	QRadioButton *m_rdNewMvt;
	QComboSCateg* m_cbNewMvt;
	qlonglong m_sctateg;
	// TODO a remplacer par un objet
	std::vector<qlonglong> m_idlist;
	std::vector<QRadioButton*> m_radioList;
	std::vector<qlonglong> m_scateglist;
};

#endif // CHOICEMVT_H

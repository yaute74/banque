#include "QDialogNotSave.h"
#include "ChoiceMvt.h"

QDialogNotSave::QDialogNotSave(QWidget *parent, QString txt) : QDialog(parent)
{
    this->setGeometry(0, 0, 700, 200);
    setWindowTitle(tr("Relevé non checkés"));
    createLayout(txt);
}

void QDialogNotSave::createLayout(QString txt)
{
	QVBoxLayout *layout = new QVBoxLayout();
    QPlainTextEdit *m_text = new QPlainTextEdit(txt);
	QDialogButtonBox *m_bouton = new QDialogButtonBox();
	layout->addWidget(m_text);
    m_bouton->setStandardButtons(QDialogButtonBox::Ok);
	m_bouton->setOrientation(Qt::Horizontal);
	layout->addWidget(m_bouton);

	setLayout(layout);

    connect(m_bouton,SIGNAL(accepted()),this,SLOT(accept()));
}


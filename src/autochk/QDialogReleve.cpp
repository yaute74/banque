#include "QDialogReleve.h"
#include "QDialogNotSave.h"
#include "ChoiceMvt.h"
#include "src/tools/QBoxCalendar.h"

QDialogReleve::QDialogReleve(QWidget *parent) : QDialog(parent)
{
	setWindowTitle(tr("Relevé de banque"));
	createLayout();
}

void QDialogReleve::createLayout()
{
	QVBoxLayout *layout = new QVBoxLayout();

    QButtonGroup *grpCheck = new QButtonGroup(this);
    m_firefox = new QRadioButton(tr("Firefox"), this);
    m_vivaldi = new QRadioButton(tr("Vivaldi"), this);
    grpCheck->addButton(m_firefox);
    grpCheck->addButton(m_vivaldi);

	QDialogButtonBox *m_bouton = new QDialogButtonBox();
	m_text = new QPlainTextEdit();
	layout->addWidget(m_text);

    layout->addWidget(m_firefox);
    layout->addWidget(m_vivaldi);
    layout->insertSpacing(3, 20);

	m_bouton->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	m_bouton->setOrientation(Qt::Horizontal);
	layout->addWidget(m_bouton);

	setLayout(layout);

    m_firefox->setChecked(true);

	connect(m_bouton,SIGNAL(accepted()),this,SLOT(accept()));
	connect(m_bouton,SIGNAL(rejected()),this,SLOT(reject()));
}

QString QDialogReleve::getPlainText()
{
	return  m_text->toPlainText();
}

bool QDialogReleve::isFirefox()
{
    return m_firefox->isChecked();
}

bool QDialogReleve::autoCheck(QWidget *parent)
{
    QString notSave("");
    QDate strdate(QDate::currentDate().year(),  QDate::currentDate().month(), 1);
    QDate enddate(QDate::currentDate().year(),  QDate::currentDate().month(), 1);

    QDate fromDate = QBoxCalendar::getDate(parent, strdate.addMonths(-1));
	if (!fromDate.isValid())
	{
		return false;
	}

    QDate toDate = QBoxCalendar::getDate(parent, enddate.addDays(-1));
	if (!toDate.isValid())
	{
		return false;
	}

	QDialogReleve dialog(parent);
	bool ret = false;
    bool quit = false;

	if (dialog.exec() == QDialog::Accepted)
	{
		QString text = dialog.getPlainText();
		QStringList lines = text.split("\n");
		bool start(false);
        int cury = QDate::currentDate().year();
        int curm = QDate::currentDate().month();

		for (int i = 0; i < lines.size(); i++)
		{
            qDebug() << lines.at(i);
			int index;
			if (!start)
			{
				//index = lines.at(i).indexOf("SOLDE AU");
				index = lines.at(i).indexOf("Graphique de répartition des dépenses");
				if (index > -1)
				{
					start = true;
                    qDebug() << "start ok";
                    if (dialog.isFirefox())
                    {
                        i++;
                    }
				}
				continue;
			}

			//index = lines.at(i).indexOf("Nouveau solde");
			index = lines.at(i).indexOf("Cliquez ici pour afficher les opérations du mois précédent...");
			if (index > -1)
			{
				break;
			}

			if (start)
			{

				QString line = lines.at(i).trimmed();
				if ( line.length() == 0)
					continue;

				bool ok;
                int d;
                int m;
                int y;

                if (dialog.isFirefox())
                {
                    d = line.mid(0, 2).toInt(&ok);
                    if (!ok)
                    {
                        qDebug() << "nok"+line;
                        continue;
                    }

                    QString mois = line.mid(2);

                    if (mois == "janv")
                        m=1;
                    else if (mois == "févr.")
                        m=2;
                    else if (mois == "mars")
                        m=3;
                    else if (mois == "avril")
                        m=4;
                    else if (mois == "mai")
                        m=5;
                    else if (mois == "juin")
                        m=6;
                    else if (mois == "juil")
                        m=7;
                    else if (mois == "août")
                        m=8;
                    else if (mois == "sept.")
                        m=9;
                    else if (mois == "oct.")
                        m=10;
                    else if (mois == "nov.")
                        m=11;
                    else if (mois == "déc.")
                        m=12;
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    QString line = lines.at(i).trimmed();
                    if ( line.length() != 2)
                    {
                        continue;
                    }
                    d = line.toInt(&ok);
                    if (!ok)
                    {
                        qDebug() << "nok"+line;
                        continue;
                    }
                    i++;
                    QString mois = lines.at(i).trimmed();

                    //QString mois = line.mid(0);

                    if (mois == "JANV.")
                        m=1;
                    else if (mois == "FÉVR.")
                        m=2;
                    else if (mois == "MARS.")
                        m=3;
                    else if (mois == "AVRIL.")
                        m=4;
                    else if (mois == "MAI.")
                        m=5;
                    else if (mois == "JUIN.")
                        m=6;
                    else if (mois == "JUIL.")
                        m=7;
                    else if (mois == "AOÛT.")
                        m=8;
                    else if (mois == "SEPT.")
                        m=9;
                    else if (mois == "OCT.")
                        m=10;
                    else if (mois == "NOV.")
                        m=11;
                    else if (mois == "DÉC.")
                        m=12;
                    else
                    {
                        continue;
                    }
                }

                if (curm <=3 && m >=10)
                {
                    y = cury-1;
                }
                else
                {
                    y = cury;
                }

				QDate mvtDate(y, m, d);
                QString sval;
				if (mvtDate.isValid())
				{
					if ((mvtDate < fromDate) || (mvtDate > toDate))
					{
						continue;
					}

					i++;
                    double val;
					//bool ok;
                    if (dialog.isFirefox())
                    {
                        sval = lines.at(i).trimmed().replace(" ", "").replace(",", ".");
                        val = sval.mid(0, sval.length()-1).toDouble(&ok);
                    }
                    else
                    {
                        index = lines.at(i).indexOf("€");
                        if (index < 0)
                        {
                            continue;
                        }
                        sval = lines.at(i).mid(0, index).trimmed().replace(" ", "").replace(",", ".");
                        //val = sval.mid(0, sval.length()-1).toDouble(&ok);
                        val = sval.toDouble(&ok);
                    }

                    if (ok)
                    {
                        QString intitule;
                        if (dialog.isFirefox())
                        {
                            i++;
                            intitule = lines.at(i);
                        }
                        else
                        {
                            intitule = lines.at(i).mid(index+2);
                        }
                        QDialogButtonBox::StandardButton choice;

                        if (!quit)
                        {
                            choice = ChoiceMvt::getMvt(&dialog, mvtDate, val, intitule);
                        }

                        if (quit || choice == QDialogButtonBox::Close)
                        {
                            quit = true;
                           // break;
                        }

                        if (quit || choice == QDialogButtonBox::Discard || choice == QDialogButtonBox::Close)
                        {
                            notSave+=mvtDate.toString()+"   "+QString::number(val)+"    "+intitule+"\n";
                            continue;
                        }
                        ret = true;
                    }
                    else
                    {
                        QMessageBox::critical(&dialog, QObject::tr("Check depuis relevé de compte"),
                                              QObject::tr("Valeur non valide:'") + sval);
                    }
				}
				else
				{
					QMessageBox::critical(&dialog, QObject::tr("Check depuis relevé de compte"),
										  QObject::tr("Date non valide:'") + QString::number(y) + "," + QString::number(m) + "," + QString::number(d));
					return ret;
				}
			}
		}
	}

    if (notSave.length() > 0)
    {
        QDialogNotSave *notSaveList = new QDialogNotSave(Q_NULLPTR, notSave);
        notSaveList->show();
    }

	return ret;
}

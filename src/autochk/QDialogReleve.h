#ifndef QDIALOGRELEVE_H
#define QDIALOGRELEVE_H

#include <QtWidgets>

class QDialogReleve : public QDialog
{
public:
	QDialogReleve(QWidget *parent = Q_NULLPTR);

	static bool autoCheck(QWidget *parent);
    bool isFirefox();
	QString getPlainText();

private:
    QRadioButton *m_firefox;
    QRadioButton *m_vivaldi;
	void createLayout();
	QPlainTextEdit *m_text;
};

#endif // QDIALOGRELEVE_H

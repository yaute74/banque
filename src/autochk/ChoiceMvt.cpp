#include "ChoiceMvt.h"
#include <QtSql>

ChoiceMvt::ChoiceMvt( QWidget *parent, QDate mvtDate, double value, QString intitule) : QDialog(parent), m_mvtDate(mvtDate), m_releveValue(value), m_releveCateg(intitule)
{
	this->setGeometry(0, 0, 600, 300);
	QRect r = this->geometry();
	r.moveCenter(QApplication::desktop()->availableGeometry().center());
	this->setGeometry(r);

    qDebug() << "!!!!!!!!!:" << m_releveValue;

	searchSCateg();
	createLayout();
}

void ChoiceMvt::createLayout()
{
	QVBoxLayout *layout = new QVBoxLayout();

	// Releve
	QGroupBox *grpBoxReleve = new QGroupBox(tr("Relevé"));
	QHBoxLayout *releveLayout = new QHBoxLayout();
	QLabel *lblReleveDate = new QLabel(m_mvtDate.toString("yyyy-MM-dd"));
	QLabel *lblReleveCateg = new QLabel(m_releveCateg);
	m_lblValue = new QLabel(QString::number(m_releveValue));
	releveLayout->addWidget(lblReleveDate);
	releveLayout->addWidget(lblReleveCateg);

	releveLayout->addWidget(m_lblValue);
	grpBoxReleve->setLayout(releveLayout);
	layout->addWidget(grpBoxReleve);

	// Mouvements
	QGroupBox *grpBoxMvt = new QGroupBox(tr("Mouvements sélection"));
	QButtonGroup *grpRadioMvt = new QButtonGroup();
	// Nouveau mouvement
	QGridLayout *newMvtLayout = new QGridLayout();
	//QRadioButton *rdNewMvt = new QRadioButton(tr("Créer"));
	m_rdNewMvt = new QRadioButton();
	m_rdNewMvt->setIcon(QIcon(":/images/add.png"));
	m_cbNewMvt = new QComboSCateg(Q_NULLPTR, false, m_sctateg);
	newMvtLayout->addWidget(m_rdNewMvt, 0, 0, 1, 1);
	newMvtLayout->addWidget(m_cbNewMvt, 0, 1, 1, 4);

	m_rdNewMvt->setChecked(true);

	grpRadioMvt->addButton(m_rdNewMvt);
	mvtList(grpRadioMvt, newMvtLayout);
	grpBoxMvt->setLayout(newMvtLayout);
	layout->addWidget(grpBoxMvt);

	// Boutons
	m_bouton = new QDialogButtonBox();
	m_bouton->setStandardButtons(QDialogButtonBox::Save | QDialogButtonBox::Discard | QDialogButtonBox::Close);
	m_bouton->setOrientation(Qt::Horizontal);
	layout->addWidget(m_bouton);

	setLayout(layout);

	connect(m_bouton, SIGNAL(clicked(QAbstractButton*)), this, SLOT(buttons(QAbstractButton*)));
}

void ChoiceMvt::searchSCateg()
{
	QString req;
	QSqlQuery q;

	req = "SELECT scategorie from releves where intitule = ?";
	q.prepare(req);
	q.addBindValue(m_releveCateg);

	if(q.exec() && q.next())
	{
		m_sctateg = q.value("scategorie").toLongLong();
	}
	else
	{
		m_sctateg = -1;
	}
}

void ChoiceMvt::mvtList(QButtonGroup *grpButtonMvt, QGridLayout *newMvtLayout)
{
	QString req;
	QSqlQuery q;
	int cpt(1);
    bool first = true;

	req = "SELECT mouvements.id, strftime('%Y', mdate) year, strftime('%m', mdate) month, strftime('%d', mdate) day, mouvements.scategorie scateg_id,";
	req +="categories.nom cnom, scategories.nom scnom, debit, prevision, valide";
	req += " from mouvements, scategories, categories";
	req += " where abs(debit) = abs(?) and";
	req += " scategories.id = mouvements.scategorie and";
	req += " scategories.categorie = categories.id and";
	req += " valide != 1";

	q.prepare(req);
	q.addBindValue(m_releveValue);

//	m_idlist = new std::vector<qlonglong>;
//	m_radioList =  new std::vector<QRadioButton*>;

//	qDebug() << req;

	if(q.exec())
	{
		while(q.next())
		{
			QRadioButton *m_radio = new QRadioButton();
			if (q.value("prevision").toInt())
			{
				m_radio->setIcon(QIcon(":/images/prev.png"));
			}
			m_idlist.push_back(q.value("id").toLongLong());
			m_scateglist.push_back(q.value("scateg_id").toLongLong());
			m_radioList.push_back(m_radio);

			QLabel *lblCateg = new QLabel(q.value("cnom").toString()+"->"+q.value("scnom").toString());

			QDate tmpDate(q.value("year").toInt(), q.value("month").toInt(), q.value("day").toInt());
			QLabel *lblDate = new QLabel(QString::number(m_mvtDate.daysTo(tmpDate))+tr(" jour(s)"));

//			qDebug() << m_mvtDate.toString();
//			qDebug() << tmpDate.toString();

//			qDebug() << q.value("debit").toString();
			QLabel *lblValue = new QLabel(QString::number(q.value("debit").toDouble()));
			QLabel *lblCheck = new QLabel();
			if (q.value("valide").toInt() != 0)
			{
				QSize size(16, 16);
				lblCheck->setPixmap(QIcon(":/images/check.png").pixmap(size));
			}
			grpButtonMvt->addButton(m_radio);
			newMvtLayout->addWidget(m_radio, cpt, 0, 1, 1);
			newMvtLayout->addWidget(lblCateg, cpt, 1, 1, 1);

			newMvtLayout->addWidget(lblDate, cpt, 2, 1, 1);
			newMvtLayout->addWidget(lblValue, cpt, 3, 1, 1);
			newMvtLayout->addWidget(lblCheck, cpt, 4, 1, 1);

            if (first)
            {
                m_radio->setChecked(true);
                first = false;
            }

			cpt++;
		}
	}
	else
	{
		QMessageBox::critical(this, QObject::tr("Check depuis relevé de compte"),
							   tr("SqLite erreur:") + q.lastError().text() + "(" + q.lastError().number() + ").\n" + tr("Requête:") + q.lastQuery());
	}

}

void ChoiceMvt::buttons(QAbstractButton *button)
{
	m_ret = m_bouton->standardButton(button);
	emit accept();
}

QDialogButtonBox::StandardButton ChoiceMvt::getRetCode()
{
	return m_ret;
}

//void ChoiceMvt::plusMoins()
//{
//	if ((m_rdPlus->isChecked() && m_releveValue < 0) || (m_rdMoins->isChecked() && m_releveValue > 0))
//	{
//		m_releveValue = -m_releveValue;
//		m_lblValue->setText(QString::number(m_releveValue));
//	}
//}

qlonglong ChoiceMvt::getMvtId()
{
	if (m_rdNewMvt->isChecked())
	{
		setReveleSCateg(getSCategId());
		return -1;
	}
	else
	{
		for (size_t i =0; i < m_radioList.size(); i++)
		{
			if (m_radioList[i]->isChecked())
			{
				setReveleSCateg(m_scateglist[i]);
				return m_idlist[i];
			}
		}
	}
	return -2;
}

qlonglong ChoiceMvt::getSCategId()
{
	return m_cbNewMvt->getSelectId();
}

double ChoiceMvt::getValue()
{
	return m_releveValue;
}

void ChoiceMvt::setReveleSCateg(qlonglong  scategId)
{
	QSqlQuery q;

	if (m_sctateg == -1)
	{
		q.prepare("insert into releves (scategorie, intitule) values (?, ?)");
		q.addBindValue(scategId);
		q.addBindValue(m_releveCateg);

	}
	else if (m_sctateg != scategId)
	{
		q.prepare("update releves set scategorie=? where intitule = ?");
		q.addBindValue(scategId);
		q.addBindValue(m_releveCateg);
	}

    if ((m_sctateg == -1) || (m_sctateg != scategId))
    {
        if(!q.exec())
        {
            QMessageBox::critical(this, QObject::tr("Check depuis relevé de compte"),
                                   tr("SqLite erreur:") + q.lastError().text() + "(" + q.lastError().number() + ").\n" + tr("Requête:") + q.lastQuery());
        }
        q.clear();
    }
}

QDialogButtonBox::StandardButton ChoiceMvt::getMvt(QWidget *parent, QDate mvtDate, double value, QString intitule)
{
	ChoiceMvt dialog(parent, mvtDate, value, intitule);

	dialog.exec();
	QDialogButtonBox::StandardButton ret = dialog.getRetCode();

	if (ret == QDialogButtonBox::Save)
	{
		QSqlQuery q;

		qlonglong mvtId = dialog.getMvtId();

		if (mvtId == -1)
		{
            q.prepare("insert into mouvements (mdate, scategorie, debit, prevision, valide) values (?, ?, ?, NULL, 2)");
			q.addBindValue(mvtDate);
			q.addBindValue(dialog.getSCategId());
			q.addBindValue(dialog.getValue());
            //q.addBindValue(QVariant::LongLong);

            qDebug() << mvtDate << "," << dialog.getSCategId() << "," << dialog.getValue();
		}
		else if (mvtId >= 0)
		{
			q.prepare("update mouvements set mdate=?, debit=?, valide=2 where id = ?");
			q.addBindValue(mvtDate);
			q.addBindValue(value);
			q.addBindValue(mvtId);

            qDebug() << mvtDate << "#" << dialog.getSCategId() << "#" << dialog.getValue();
		}

		if(!q.exec())
		{
			QMessageBox::critical(&dialog, QObject::tr("Check depuis relevé de compte"),
								   tr("SqLite erreur:") + q.lastError().text() + "(" + q.lastError().number() + ").\n" + tr("Requête:") + q.lastQuery());
			ret =  QDialogButtonBox::Discard;
		}
		q.clear();
	}
	return ret;
}

void ChoiceMvt::closeEvent(QCloseEvent *event)
{
	m_ret = QDialogButtonBox::Close;
	event->accept();
}

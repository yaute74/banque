#ifndef QDIALOGNOTSAVE_H
#define QDIALOGNOTSAVE_H

#include <QtWidgets>

class QDialogNotSave : public QDialog
{
public:
    QDialogNotSave(QWidget *parent = Q_NULLPTR, QString txt = Q_NULLPTR);

private:
    void createLayout(QString txt);

};

#endif // QDIALOGNOTSAVE_H

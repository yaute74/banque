#include "FrameFilter.h"

FrameFilter::FrameFilter(QWidget *parent) : QGroupBox(parent)
{
	m_bindvalues = new std::vector<QVariant>();
	createLayout();
}

void FrameFilter::createLayout()
{
	QVBoxLayout *layout = new QVBoxLayout();

	QButtonGroup *grpCheck = new QButtonGroup(this);
	m_checkYes = new QRadioButton(tr("Avec checks"), this);
	m_checkNo = new QRadioButton(tr("Sans checks"), this);
	m_checkYesNo = new QRadioButton(tr("Both"), this);
	QLabel *lblDebit = new QLabel(tr("Débit:"));
	m_debit = new QDoubleSpinBox(this);
	m_tvCateg = new QTreeView(this);
	QPushButton *btFiltrer = new QPushButton(tr("Filtrer"));
	QPushButton *btCleanFiltrer = new QPushButton(tr("Nettoyer"));

	m_debit->setMinimum(-999999.99);
	m_debit->setMaximum(999999.99);

	grpCheck->addButton(m_checkYesNo);
	grpCheck->addButton(m_checkYes);
	grpCheck->addButton(m_checkNo);

	layout->setAlignment(Qt::AlignTop);
	//layout->setSpacing(10);

	layout->addWidget(m_checkYes);
	layout->addWidget(m_checkNo);
	layout->insertSpacing(3, 20);

	QHBoxLayout *debLayout = new QHBoxLayout();
	debLayout->addWidget(lblDebit);
	debLayout->insertSpacing(1, 10);
	debLayout->addWidget(m_debit);
	layout->addLayout(debLayout);

	layout->insertSpacing(4, 20);
	m_tvCateg->setMaximumHeight(300);
	layout->addWidget(m_tvCateg);
	layout->insertSpacing(5, 20);

	QHBoxLayout *btLayout = new QHBoxLayout();
	btLayout->addWidget(btFiltrer);
	btLayout->insertSpacing(1, 10);
	btLayout->addWidget(btCleanFiltrer);

	layout->addLayout(btLayout);
	layout->insertSpacing(7, 50);
	setLayout(layout);

	m_tvCateg->setSelectionMode(QAbstractItemView::MultiSelection);
	m_model = new CategManageModel(this, true);
	m_tvCateg->setModel(m_model);

	m_checkYesNo->setVisible(false);
	m_checkYesNo->setChecked(true);

	connect(btFiltrer, SIGNAL(clicked()), parent(), SLOT(setFilter()));
	connect(btCleanFiltrer, SIGNAL(clicked()), this, SLOT(initWidgets()));
}

void FrameFilter::initWidgets()
{
	m_checkYesNo->setChecked(true);
	m_debit->setValue(0);
	m_tvCateg->clearSelection();
}

bool FrameFilter::isFilter()
{
	if (!m_checkYesNo->isChecked() || m_debit->value() != 0 || m_tvCateg->selectionModel()->selection().indexes().count() > 0)
	{
		return true;
	}
	return false;
}
QString FrameFilter::getFilter()
{
	QString req("");
	m_bindvalues->clear();

	if (!m_checkYesNo->isChecked() || m_debit->value() != 0 || m_tvCateg->selectionModel()->selection().indexes().count() > 0)
	{
		if (m_checkNo->isChecked())
		{
			req+=" valide=?";
			m_bindvalues->push_back(0);
		}
		else if (m_checkYes->isChecked())
		{
			req+=" valide<>?";
			m_bindvalues->push_back(0);
		}

		if (m_debit->value() != 0)
		{
			if (req.length() > 0)
			{
				req+=" and";
			}
			req+=" debit=?";
			m_bindvalues->push_back(m_debit->value());
		}

		QString ids = m_model->getIdList(m_tvCateg->selectionModel()->selection().indexes());
		if (ids.length() > 0)
		{
			if (req.length() > 0)
			{
				req+=" and";
			}
			req+=" scategorie in ("+ids+")";
		}

		qDebug() << req;
		return req;
	}
	else
	{
		return NULL;
	}
}

std::vector<QVariant> *FrameFilter::getBindValues()
{
	return m_bindvalues;
}

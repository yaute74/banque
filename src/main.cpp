#include "MainUI.h"
#include <QtWidgets>
#include <QApplication>
#include <QtSql>
#include <sys/stat.h>

/* TODO
 * -demander confirmation d'enregistrer avant de faire un filtre
 * -gérer les filtres
 * -gérer les stats
 * -lorsqu'on modifie les catégories (nom de la souscategorie), rafraichier la liste des mouvements ?
 *
 * -ajouter - en saisie du debit
 */

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QString banque;
	int ret;
	MainUI *w;
	struct stat buffer;

	if (argc == 2)
	{
		banque = argv[1];

		if (stat(banque.toStdString().c_str(), &buffer))
		{
			QMessageBox::critical(NULL, QObject::tr("Base de données"), QObject::tr("Fichier ou répertoire de base de données '") + banque + QObject::tr("' non trouvé"));
			exit(1);
		}
	}

	if (!banque.length() || S_ISDIR(buffer.st_mode))
	{
		banque = QFileDialog::getOpenFileName(NULL, QObject::tr("Base de données"), banque, "SQLite (*.db)");
	}

	if (!banque.length())
	{
		exit(1);
	}

	QFile::copy(banque, banque+"_"+QDateTime::currentDateTime().toString("yyyy-MM-dd_hh.mm.ss"));

	QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
	db.setDatabaseName(banque);

	//qDebug() << QCoreApplication::libraryPaths();

	if(!db.open())
	{
		QMessageBox::critical(NULL, QObject::tr("Base de données"), QObject::tr("La connexion a échouée:") + db.lastError().text());
		exit(1);
	}

	QSqlQuery q;
	q.exec("PRAGMA foreign_keys = ON;");
	q.clear();


	int minYear;
	q.clear();
	if(q.exec("select min(strftime('%Y', mdate)) min_year from mouvements where mdate>Datetime('2000-01-01 00:00:00')") && q.next())
	{
		minYear = q.value("min_year").toInt();
	}
	else
	{
		minYear = QDate::currentDate().year();
	}


	a.setWindowIcon(QIcon(":/images/logo.png"));
	try
	{
		w = new MainUI(banque, minYear);
	}
	catch(QString const& chaine)
	{
		QMessageBox::critical(NULL, "Erreur SQL", chaine);
		db.close();
		exit(1);
	}

	QString locale = QLocale::system().name().section('_', 0, 0);
	QTranslator translator;
	translator.load(QString("qt_") + locale, QLibraryInfo::location(QLibraryInfo::TranslationsPath));
	a.installTranslator(&translator);

	w->show();
	ret = a.exec();
	db.close();
	return ret;
}


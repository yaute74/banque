#include "PrevObj.h"

PrevObj::PrevObj(qlonglong id, qlonglong scategrie, int updtype, qlonglong jour, double janvier, double fevrier, double mars , double avril , double mai , double juin ,
				 double juillet, double aout, double septembre, double octobre, double novembre, double decembre) : m_id(id), m_scategrie(scategrie), m_jour(jour), m_update(updtype)
{
	m_data.push_back(janvier);
	m_data.push_back(fevrier);
	m_data.push_back(mars);
	m_data.push_back(avril);
	m_data.push_back(mai);
	m_data.push_back(juin);
	m_data.push_back(juillet);
	m_data.push_back(aout);
	m_data.push_back(septembre);
	m_data.push_back(octobre);
	m_data.push_back(novembre);
	m_data.push_back(decembre);
}

qlonglong PrevObj::getId()
{
	return m_id;
}

qlonglong PrevObj::getIDCateg()
{
	return m_scategrie;
}

qlonglong PrevObj::getJour()
{
	return m_jour;
}

double PrevObj::getMonth(int month)
{
	return m_data[month];
}

void PrevObj::setIDCateg(qlonglong value)
{
	if (m_scategrie != value)
	{
		m_scategrie = value;
		if (m_update == LINE_NOCHANGE)
		{
			m_update = LINE_UDP;
		}
	}
}

void PrevObj::setJour(qlonglong value)
{
	if (m_jour != value)
	{
		m_jour = value;
		if (m_update == LINE_NOCHANGE)
		{
			m_update = LINE_UDP;
		}
	}
}

void PrevObj::setMonth(int month, double value)
{
	if (m_data[month] != value)
	{
		m_data[month] = value;
		if (m_update == LINE_NOCHANGE)
		{
			m_update = LINE_UDP;
		}
	}
}

int PrevObj::getUpdate()
{
	return m_update;
}

void PrevObj::setUpdate(int value)
{
	if (value == LINE_DELETE || value == LINE_NOCHANGE || value == LINE_UDP || value == LINE_INSERT)
		m_update = value;
}

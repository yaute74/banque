#include "PrevModel.h"
#include "src/scateg/SCategTables.h"
#include <QtSql>

PrevModel::PrevModel(QObject *parent) : QAbstractTableModel(parent)
{
	loadPrevisions();
}

int PrevModel::rowCount(const QModelIndex &) const
{
	return m_data.size();
}

int PrevModel::columnCount(const QModelIndex &) const
{
	return 14;
}

QVariant PrevModel::data(const QModelIndex &index, int role) const
{
	int row = index.row();
	int col = index.column();

	switch(role){
	case Qt::DisplayRole:
		switch(col)
		{
		/*case COL_ID:
			return m_data[row]->getId();
			break;*/
		case COL_CATEG:
			return SCategTables::getSoucategById()->at(m_data[row]->getIDCateg());
			break;
		case COL_JOUR:
			return m_data[row]->getJour();
			break;
		default:
			return m_data[row]->getMonth(col-2);
			break;
		}
		break;
	case Qt::EditRole:
		switch(col)
		{
		case COL_CATEG:
		case COL_JOUR:
			break;
		default:
			return m_data[row]->getMonth(col-2);
			break;
		}
		break;
	case Qt::BackgroundRole:
		switch(m_data[row]->getUpdate())
		{
		case PrevObj::LINE_NOCHANGE:
			break;
		case PrevObj::LINE_UDP:
		{
			QBrush updline(QColor(255,244,207));
			return updline;
		}
			break;
		case PrevObj::LINE_INSERT:
		{
			QBrush newline(QColor(255,231,207));
			return newline;
		}
			break;
		case PrevObj::LINE_DELETE:
		{
			QBrush delline(QColor(205,205,205));
			return delline;
		}
			break;
		}
		break;
	}
	return QVariant();
}

QVariant PrevModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal)
	{
		if (role == Qt::DisplayRole)
		{
			switch (section)
			{
			/*case COL_ID:
				return QString(tr("Id"));
				break;*/
			case COL_CATEG:
				return QString(tr("Catégorie"));
				break;
			case COL_JOUR:
				return QString(tr("Jour"));
				break;
			default:
				return QDate::shortMonthName(section-1);
				break;
			}
		}
		else if (role == Qt::SizeHintRole)
		{
			QSize size(100, 25);
			switch (section)
			{
			/*case COL_ID:
				size.setWidth(20);
				break;*/
			case COL_CATEG:
				size.setWidth(150);
				break;
			case COL_JOUR:
				size.setWidth(35);
				break;
			default:
				size.setWidth(60);
				break;
			}
			return QVariant(size);
		}
	}

	return QVariant();
}

bool PrevModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	int row = index.row();
	int col = index.column();

	if (role == Qt::EditRole)
	{
		switch(col)
		{
		case COL_CATEG:
			qDebug() << value.toLongLong();
			m_data[row]->setIDCateg(value.toLongLong());
			break;
		case COL_JOUR:
			if (value.toLongLong()>31)
				return false;
			m_data[row]->setJour(value.toLongLong());
			break;
		default:
			m_data[row]->setMonth(col-2, value.toDouble());
			break;
		}
		refreshColorLine(row);
		return true;
	}
	return true;
}

bool PrevModel::insertRow(int row, const QModelIndex &)
{
	beginInsertRows(QModelIndex(), row, row);
	endInsertRows();
	return true;
}

Qt::ItemFlags PrevModel::flags(const QModelIndex &index) const
{
	return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable;
/*
	if (index.column() != COL_ID)
	{
		flags |= ;
	}

	return flags;*/
}

void PrevModel::refreshColorLine(int row)
{
	QModelIndex top = createIndex(row, 0);
	QModelIndex bottom = createIndex(row, columnCount()-1);
	QVector<int> role;
	role.push_back(Qt::BackgroundRole);
	emit dataChanged(top, bottom, role);
}

void PrevModel::loadPrevisions()
{
	QString req;
	QSqlQuery query;//(QSqlDatabase::database());

	req = "select previsionnel.id, previsionnel.scategorie, previsionnel.jour, previsionnel.janvier, previsionnel.fevrier,";
	req += "previsionnel.mars , previsionnel.avril , previsionnel.mai , previsionnel.juin , previsionnel.juillet,";
	req += "previsionnel.aout, previsionnel.septembre, previsionnel.octobre, previsionnel.novembre, previsionnel.decembre";
	req += " from previsionnel,scategories, categories ";
	req += " where previsionnel.scategorie=scategories.id and scategories.categorie=categories.id ";
	req += " order by categories.nom,scategories.nom";

	query.prepare(req);

	if(query.exec())
	{
		while(query.next())
		{
			m_data.push_back(new PrevObj(query.value("id").toLongLong(), query.value("scategorie").toLongLong(), PrevObj::LINE_NOCHANGE, query.value("jour").toLongLong(), query.value("janvier").toDouble(),
										 query.value("fevrier").toDouble(), query.value("mars").toDouble(), query.value("avril").toDouble(), query.value("mai").toDouble(),
										 query.value("juin").toDouble(), query.value("juillet").toDouble(), query.value("aout").toDouble(), query.value("septembre").toDouble(),
										 query.value("octobre").toDouble(), query.value("novembre").toDouble(), query.value("decembre").toDouble()));

		}
	}
	else
	{
		throw QString("Erreur SQL: " + query.lastError().text() + ".\n Request:" + query.lastQuery());
	}
}

QModelIndex PrevModel::addLine()
{
	std::vector<PrevObj*>::iterator it;
	int newrow = 0;
	PrevObj *newLine;

	newLine = new PrevObj(-1, SCategTables::getSoucategById()->begin()->first, PrevObj::LINE_INSERT);
/*
	for (it = m_data.begin(); it != m_data.end(); it++)
	{
		if ((*it)->getDate() > date)
		{
			m_data.insert(it, newLine);
			break;
		}
		newrow++;
	}
*/
//	if (m_data.size() == 0 || it == m_data.end())
	{
		m_data.push_back(newLine);
		newrow = rowCount()-1;
	}

	insertRow(newrow);
	return index(newrow, COL_CATEG);
}

bool PrevModel::saveLines()
{
	std::vector<PrevObj*>::iterator it;
	QSqlQuery q;//(QSqlDatabase::database());

	for (it = m_data.begin(); it != m_data.end(); ++it)
	{
		switch((*it)->getUpdate())
		{
		case PrevObj::LINE_DELETE:
			if ((*it)->getId() == -1)
			{
				continue;
			}
			q.prepare("delete from previsionnel where id=?");
			q.addBindValue((*it)->getId());
			break;
		case PrevObj::LINE_UDP:
			q.prepare("update previsionnel set scategorie=?, jour=?, janvier=?, fevrier=?,mars =?, avril =?, mai =?, juin =?, juillet=?, aout=?, septembre=?, octobre=?, novembre=?, decembre=? where id=?");
			q.addBindValue((*it)->getIDCateg());
			q.addBindValue((*it)->getJour());
			for (int i=0; i <12; i++)
			{
				q.addBindValue((*it)->getMonth(i));
			}
			q.addBindValue((*it)->getId());

			break;
		case PrevObj::LINE_INSERT:
			q.prepare("insert into previsionnel (scategorie, jour, janvier, fevrier, mars, avril, mai ,juin ,juillet, aout, septembre, octobre, novembre, decembre) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			q.addBindValue((*it)->getIDCateg());
			q.addBindValue((*it)->getJour());
			for (int i=0; i <12; i++)
			{
				q.addBindValue((*it)->getMonth(i));
			}
			break;
		default:
			continue;
			break;
		}

		if(!q.exec())
		{
			m_lastError = tr("SqLite erreur:") + q.lastError().text() + "(" + q.lastError().number() + ").\n" + tr("Requête:") + q.lastQuery();
			return false;
		}

		q.clear();
	}
	return true;
}

bool PrevModel::mustSave()
{
	for (size_t i = 0; i < m_data.size(); i++)
	{
		if (m_data[i]->getUpdate() != PrevObj::LINE_NOCHANGE)
		{
			return true;
		}
	}
	return false;
}

void PrevModel::delLine(int row)
{
	m_data[row]->setUpdate(PrevObj::LINE_DELETE);
	refreshColorLine(row);
}

void PrevModel::copyMonthValue(int row, int colFrom)
{
	double value = m_data[row]->getMonth(colFrom-2);

	qDebug() << QString::number(row);
	qDebug() << QString::number(colFrom);
	qDebug() << QString::number(value);

	for (int i=0; i <12; i++)
	{
		if (i!=colFrom-2)
		{
			m_data[row]->setMonth(i, value);
		}
	}
	refreshColorLine(row);
}

QString PrevModel::getLastError()
{
	return m_lastError;
}

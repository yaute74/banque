#ifndef PREVTABLE_H
#define PREVTABLE_H

#include <QtWidgets>

class PrevTable : public QTableView
{
public:
	PrevTable(QWidget *parent = Q_NULLPTR);
	void commitCloseEditor();
};

#endif // PREVTABLE_H

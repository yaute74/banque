#include "PrevTable.h"

PrevTable::PrevTable(QWidget *parent) : QTableView(parent)
{

}

void PrevTable::commitCloseEditor()
{
	QModelIndex index = currentIndex();
	if (index.isValid())
	{
		QWidget *editor = indexWidget(index);
		if (editor != NULL)
		{
			commitData(editor) ;
			closeEditor(editor,  QAbstractItemDelegate::NoHint);
		}
	}
}

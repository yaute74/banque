#ifndef PREVOBJ_H
#define PREVOBJ_H

#include <QtWidgets>

class PrevObj
{
public:
	enum { LINE_NOCHANGE=0, LINE_UDP, LINE_INSERT, LINE_DELETE};

	PrevObj(qlonglong id, qlonglong scategrie, int updtype=LINE_NOCHANGE, qlonglong jour=1, double janvier=0, double fevrier=0, double mars=0, double avril=0, double mai=0, double juin=0,
			double juillet=0, double aout=0, double septembre=0, double octobre=0, double novembre=0, double decembre=0);

	qlonglong getId();
	qlonglong getIDCateg();
	qlonglong getJour();
	void setIDCateg(qlonglong value);
	void setJour(qlonglong value);
	void setMonth(int month, double value);
	double getMonth(int month);
	int getUpdate();
	void setUpdate(int value);

private:
	qlonglong m_id;
	qlonglong m_scategrie;
	qlonglong m_jour;
	std::vector<double> m_data;
	int m_update;
};

#endif // PREVOBJ_H

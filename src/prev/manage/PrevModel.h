#ifndef PREVMODEL_H
#define PREVMODEL_H

#include "PrevObj.h"
#include <QtWidgets>

class PrevModel : public QAbstractTableModel
{
public:
	PrevModel(QObject *parent = Q_NULLPTR);
	int rowCount(const QModelIndex &parent = QModelIndex()) const ;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const;
	bool setData(const QModelIndex & index, const QVariant & value, int role);
	bool insertRow(int row, const QModelIndex &parent = QModelIndex());
	Qt::ItemFlags flags(const QModelIndex &) const;
	//QSize span(const QModelIndex &index) const;
	QModelIndex addLine();
	bool saveLines();
	bool mustSave();
	void delLine(int row);
	void copyMonthValue(int row, int colFrom);
	QString getLastError();

	//enum {COL_ID=0, COL_CATEG, COL_JOUR};
	enum {COL_CATEG=0, COL_JOUR};

private:
	void refreshColorLine(int row);
	void loadPrevisions();
	PrevObj *test;
	std::vector<PrevObj *> m_data;
	QString m_lastError;
};

#endif // PREVMODEL_H

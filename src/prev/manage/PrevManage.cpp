#include "PrevManage.h"
#include "src/scateg/SCategCbDelegate.h"

PrevManage::PrevManage(QWidget *parent) : QMainWindow(parent)
{
	this->setGeometry(0, 0, 1000, 600);
	this->setWindowTitle(tr("Prévisions"));

	// Center frame
	QRect r = this->geometry();
	r.moveCenter(QApplication::desktop()->availableGeometry().center());
	this->setGeometry(r);

	createMenus();
	createLayout();
}

void PrevManage::createMenus()
{
	QAction *actMvtAdd = new QAction(QIcon(":/images/add.png"), tr("Ajoute"), this);
	QAction *actMvtSave = new QAction(QIcon(":/images/save.png"), tr("Enregistrer"), this);
	QAction *actMvtDel = new QAction(QIcon(":/images/del.png"), tr("Supprimer"), this);
	QToolBar *toolBarMain = addToolBar("Main");

	toolBarMain->addAction(actMvtAdd);
	toolBarMain->addAction(actMvtSave);
	toolBarMain->addAction(actMvtDel);

	connect(actMvtAdd, SIGNAL(triggered()), this, SLOT(addLine()));
	connect(actMvtSave, SIGNAL(triggered()), this, SLOT(saveLines()));
	connect(actMvtDel, SIGNAL(triggered()), this, SLOT(delLine()));
}

void PrevManage::createLayout()
{
	m_table = new PrevTable(this);
	m_model = new PrevModel(this);
	m_table->setModel(m_model);
	//m_table->setSpan(m_tabbleModel->span());
	//m_table->setSpan(0,1,2,1);
	m_table->setSelectionMode(QAbstractItemView::SingleSelection);
	m_table->resizeColumnsToContents();
	m_table->setItemDelegateForColumn(PrevModel::COL_CATEG, new SCategCbDelegate(this));
	m_table->setContextMenuPolicy(Qt::CustomContextMenu);

	setCentralWidget(m_table);
	connect(m_table, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(showContextMenu(const QPoint&)));
}

void PrevManage::showContextMenu(const QPoint& pos)
{
	QModelIndexList indexes = m_table->selectionModel()->selection().indexes();
	if (indexes.count() > 0 && indexes.at(0).column() > PrevModel::COL_JOUR)
	{
		QPoint globalPos = m_table->mapToGlobal(pos);
		QMenu menucp;
		menucp.addAction(tr("Copier"));

		QAction* selectedItem = menucp.exec(globalPos);
		if (selectedItem)
		{
			m_model->copyMonthValue(indexes.at(0).row(), indexes.at(0).column());
		}
	}
}

void PrevManage::addLine()
{
	QModelIndex index = m_model->addLine();
	 m_table->selectionModel()->select(index, QItemSelectionModel::Select);
	 m_table->scrollTo(index);
}

bool PrevManage::saveLines()
{
	m_table->commitCloseEditor();

	if (m_model->saveLines())
	{
		delete m_model;
		createLayout();
	}
	else
	{
		QMessageBox::critical(this, tr("Erreur"), m_model->getLastError());
		return false;
	}
	return true;
}

void PrevManage::delLine()
{
	QModelIndexList indexes = m_table->selectionModel()->selection().indexes();
	if (indexes.count() > 0)
	{
		m_model->delLine(indexes.at(0).row());
	}
}

void PrevManage::closeEvent(QCloseEvent *event)
{
	m_table->commitCloseEditor();

	if (m_model->mustSave())
	{
		int reponse = QMessageBox::question(this, tr("Modifications"), tr("Sauvegarder les modifications ?"), QMessageBox ::Yes | QMessageBox::No | QMessageBox::Cancel);

		if (reponse == QMessageBox::Yes)
		{
			if (!saveLines())
			{
				event->ignore();
				return;
			}
		}
		else if (reponse == QMessageBox::Cancel)
		{
			event->ignore();
			return;
		}
	}

	event->accept();
}

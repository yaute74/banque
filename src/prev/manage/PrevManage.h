#ifndef PREVMANAGE_H
#define PREVMANAGE_H

#include "PrevModel.h"
#include "PrevTable.h"
#include <QtWidgets>

class PrevManage : public QMainWindow
{
		Q_OBJECT
public:
	PrevManage(QWidget *parent = Q_NULLPTR);

public slots:
	void addLine();
	bool saveLines();
	void delLine();
	void showContextMenu(const QPoint& pos);

protected:
	void closeEvent(QCloseEvent *event);

private:
	void createMenus();
	void createLayout();

	PrevModel *m_model;
	PrevTable *m_table;
	//QMenu *m_menucp;
};

#endif // PREVMANAGE_H

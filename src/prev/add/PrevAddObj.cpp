#include "PrevAddObj.h"

PrevAddObj::PrevAddObj(qlonglong id, double debit, qlonglong jour, qlonglong scategorie) : m_id(id), m_debit(debit), m_jour(jour), m_scatgegorie(scategorie)
{

}

qlonglong PrevAddObj::getId()
{
	return m_id;
}

double PrevAddObj::getDebit()
{
	return m_debit;
}

qlonglong PrevAddObj::getJour()
{
	return m_jour;
}

qlonglong PrevAddObj::getScategorie()
{
	return m_scatgegorie;
}

PrevAddObj::~PrevAddObj()
{
}

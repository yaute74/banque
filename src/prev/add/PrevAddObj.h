#ifndef PREVADDOBJ_H
#define PREVADDOBJ_H

#include <QtWidgets>

class PrevAddObj
{
public:
	PrevAddObj(qlonglong id, double debit, qlonglong jour, qlonglong m_scatgegorie);
	qlonglong getId();
	double getDebit();
	qlonglong getJour();
	qlonglong getScategorie();
	~PrevAddObj();

private:
	qlonglong m_id;
	double m_debit;
	qlonglong m_jour;
	qlonglong m_scatgegorie;
};

#endif // PREVADDOBJ_H

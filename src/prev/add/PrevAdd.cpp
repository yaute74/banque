#include "PrevAdd.h"
#include <QtSql>

PrevAdd::PrevAdd(QWidget *parent, std::set<qlonglong> &prevInCache) : QDialog(parent), m_prevInCache(&prevInCache)
{
	createLayout();
	loadCombo();
}

void PrevAdd::createLayout()
{
	m_layout = new QGridLayout();
	m_bouton = new QDialogButtonBox();
	m_list = new QTableWidget();
	m_cbmonth = new QComboBox();
	m_layout->addWidget(m_cbmonth,1,0,1,1);
	m_layout->addWidget(m_list,2,0,1,1);
	m_bouton->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	m_bouton->setOrientation(Qt::Horizontal);
	m_layout->addWidget(m_bouton,3,0,1,1);
	m_list->verticalHeader()->setVisible(false);
	m_list->horizontalHeader()->setVisible(false);
	m_list->horizontalHeader()->setStretchLastSection(true);
	m_list->setSelectionBehavior(QAbstractItemView::SelectRows);
	this->setLayout(m_layout);

	connect(m_bouton,SIGNAL(accepted()),this,SLOT(accept()));
	connect(m_bouton,SIGNAL(rejected()),this,SLOT(reject()));
	connect(m_cbmonth,SIGNAL(currentIndexChanged(int)),this,SLOT(changeMonth(int)));
}

void PrevAdd::loadCombo()
{
	int curMonth = QDate::currentDate().month();
	int curYear = QDate::currentDate().year();
	QString item;

	for (int i = 1; i <=12; i++)
	{
		switch(i)
		{
		case 1:
			item = tr("Janvier");
			break;
		case 2:
			item = tr("Février");
			break;
		case 3:
			item = tr("Mars");
			break;
		case 4:
			item = tr("Avril");
			break;
		case 5:
			item = tr("Mai");
			break;
		case 6:
			item = tr("Juin");
			break;
		case 7:
			item = tr("Juillet");
			break;
		case 8:
			item = tr("Août");
			break;
		case 9:
			item = tr("Septembre");
			break;
		case 10:
			item = tr("Octobre");
			break;
		case 11:
			item = tr("Novembre");
			break;
		case 12:
			item = tr("Décembre");
			break;
		}

		if (curMonth <= i)
		{
			m_cbmonth->addItem(item + " - " + QString::number(curYear));
		}
		else
		{
			m_cbmonth->addItem(item + " - " + QString::number(curYear+1));
		}
	}
	m_cbmonth->setCurrentIndex((QDate::currentDate().addMonths(1).month())-1);
}


void PrevAdd::changeMonth(int month)
{
	QSqlQuery query;//(QSqlDatabase::database());
	QString req;
	QString monthName;

	m_year = QDate::currentDate().year();
	m_month = month + 1;

	if (QDate::currentDate().month() > m_month)
	{
		m_year++;
	}


	m_list->setRowCount(0);
	//m_list->clear();
	m_data.clear();
	m_list->setColumnCount(3);
	m_list->setColumnWidth(0, 25);
	m_list->setColumnWidth(1, 160);
	m_list->setColumnWidth(2, 35);

	switch(m_month)
	{
	case 1:
		monthName ="janvier";
		break;
	case 2:
		monthName ="fevrier";
		break;
	case 3:
		monthName ="mars";
		break;
	case 4:
		monthName ="avril";
		break;
	case 5:
		monthName ="mai";
		break;
	case 6:
		monthName ="juin";
		break;
	case 7:
		monthName ="juillet";
		break;
	case 8:
		monthName ="aout";
		break;
	case 9:
		monthName ="septembre";
		break;
	case 10:
		monthName ="octobre";
		break;
	case 11:
		monthName ="novembre";
		break;
	case 12:
		monthName ="decembre";
		break;
	}

	req = "select categories.nom cnom,scategories.nom scnom, previsionnel.id, previsionnel.jour, previsionnel.scategorie, previsionnel."+monthName+" debit";
	req += " from previsionnel,categories,scategories";
	req += " where previsionnel.scategorie=scategories.id and scategories.categorie=categories.id and ";
	req += monthName + "!=?";
	req +=" and previsionnel.id not in (select prevision from mouvements where strftime('%Y',mdate) = ? and strftime('%m',mdate) = ? and prevision is not null) order by previsionnel.jour";
	query.prepare(req);
	query.addBindValue(0);
	query.addBindValue(QString::number(m_year));
	query.addBindValue(QString("%1").arg(m_month, 2, 10, QChar('0')));

	qDebug() << req;

	if(query.exec())
	{
		while(query.next())
		{
			if (m_prevInCache->find(query.value("id").toLongLong()) != m_prevInCache->end())
				continue;
			QTableWidgetItem *itemJour = new QTableWidgetItem(query.value("jour").toString());
			itemJour->setFlags(itemJour->flags() & ~Qt::ItemIsEditable);
			QTableWidgetItem *itemCateg = new QTableWidgetItem(query.value("cnom").toString()+"->"+query.value("scnom").toString());
			itemCateg->setFlags(itemCateg->flags() & ~Qt::ItemIsEditable);
			QTableWidgetItem *itemDebit = new QTableWidgetItem(query.value("debit").toString());
			itemDebit->setFlags(itemDebit->flags() & ~Qt::ItemIsEditable);

			//qDebug() << query.value("previsionnel."+monthName);

			qDebug() << query.value("debit").toString();
			qDebug() << query.value(5).toString();
			//query.value();

			m_list->insertRow(m_list->rowCount());
			m_data.push_back(new PrevAddObj(query.value("id").toLongLong(), query.value("debit").toDouble(), query.value("jour").toLongLong(),
										 query.value("scategorie").toLongLong()));
			m_list->setItem(m_list->rowCount()-1, 0, itemJour);
			m_list->setItem(m_list->rowCount()-1, 1, itemCateg);
			m_list->setItem(m_list->rowCount()-1, 2, itemDebit);
		}
	}
	else
	{
		throw QString("Erreur SQL: " + query.lastError().text() + ".\n Request:" + query.lastQuery());
	}

	m_list->selectAll();
}

std::vector<PrevAddObj*> PrevAdd::getPrevList()
{
	QList<QTableWidgetItem *> selectList = m_list->selectedItems();

	for (int i = m_data.size()-1; i >= 0 ; i--)
	{
		if (!m_list->item(i, 0)->isSelected())
		{
			m_data.erase(m_data.begin()+i);
		}
	}

	return m_data;
}

int PrevAdd::getYear()
{
	return m_year;
}

int PrevAdd::getMonth()
{
	return m_month;
}

PrevAdd::~PrevAdd()
{
	delete m_layout;
	delete m_bouton;
	delete m_list;
	delete m_cbmonth;

	for (size_t i = 0; i < m_data.size(); i++)
	{
		delete m_data[i];
	}
}

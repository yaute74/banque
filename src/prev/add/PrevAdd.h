#ifndef PREVADD_H
#define PREVADD_H

#include "PrevAddObj.h"
#include <QtWidgets>
#include <vector>
#include <set>

class PrevAdd : public QDialog
{
	Q_OBJECT

public:
	PrevAdd(QWidget *parent, std::set<qlonglong> &prevInCache);
	std::vector<PrevAddObj*> getPrevList();
	int getYear();
	int getMonth();
	~PrevAdd();

public slots:
	void changeMonth(int month);
	//void filldata();

private:
	void createLayout();
	void loadCombo();

	QGridLayout *m_layout;
	QDialogButtonBox *m_bouton;
	QTableWidget *m_list;
	QComboBox *m_cbmonth;
	std::vector<PrevAddObj*> m_data;
	int m_month;
	int m_year;
	std::set<qlonglong> *m_prevInCache;
};

#endif // PREVADD_H

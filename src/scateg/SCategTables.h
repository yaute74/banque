#ifndef SCATEGTABLES_H
#define SCATEGTABLES_H

#include <QtWidgets>

class SCategTables
{
public:
	SCategTables();

	static std::map<qlonglong, QString> *getSoucategById();
	static std::map<QString, qlonglong> *getSoucategByName();
	static void refresh();

private:
	void load();

	static std::map<qlonglong, QString> m_scateg_by_id;
	static std::map<QString, qlonglong> m_scateg_by_name;
	static SCategTables *instance;
};

#endif // SCATEGTABLES_H

#include "CategManageModel.h"
#include "SCategTables.h"
#include "CategItem.h"
#include <QtSql>


CategManageModel::CategManageModel(QObject *parent, bool readOnly)
	: QAbstractItemModel(parent), m_readOnly(readOnly)
{
	rootItem = new CategItem("", -1);
	loadCategories();
}

CategManageModel::~CategManageModel()
{
	delete rootItem;
}

int CategManageModel::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid())
		return static_cast<CategItem*>(parent.internalPointer())->columnCount();
	else
		return rootItem->columnCount();
}

QVariant CategManageModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	CategItem *item = static_cast<CategItem*>(index.internalPointer());

	switch(role)
	{
	case Qt::DisplayRole:
	case Qt::EditRole:
		return item->getNom();
		break;
	case Qt::BackgroundRole:
		switch(item->getUpdate())
		{
		case CategItem::LINE_NOCHANGE:
			break;
		case CategItem::LINE_UDP:
		{
			QBrush updline(QColor(255,244,207));
			return updline;
		}
			break;
		case CategItem::LINE_INSERT:
		{
			QBrush newline(QColor(255,231,207));
			return newline;
		}
			break;
		case CategItem::LINE_DELETE:
		{
			QBrush delline(QColor(205,205,205));
			return delline;
		}
			break;
		}
		break;
	}

	return QVariant();
}

bool CategManageModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (!index.isValid())
		return false;

	if (role == Qt::EditRole)
	{
		CategItem *item = static_cast<CategItem*>(index.internalPointer());
		item->setNom(value.toString());
	}
	return true;
}

Qt::ItemFlags CategManageModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return 0;

	CategItem *item = static_cast<CategItem*>(index.internalPointer());

	if (m_readOnly)
	{
		if (!index.isValid() || item->parent() == rootItem)
			return Qt::ItemIsEnabled;
		else
			return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	}
	else
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

QVariant CategManageModel::headerData(int, Qt::Orientation orientation,
							   int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
		return rootItem->getNom();

	return QVariant();
}

QModelIndex CategManageModel::index(int row, int column, const QModelIndex &parent)
			const
{
	if (!hasIndex(row, column, parent))
		return QModelIndex();

	CategItem *parentItem;

	if (!parent.isValid())
		parentItem = rootItem;
	else
		parentItem = static_cast<CategItem*>(parent.internalPointer());

	CategItem *childItem = parentItem->child(row);
	if (childItem)
		return createIndex(row, column, childItem);
	else
		return QModelIndex();
}

QModelIndex CategManageModel::parent(const QModelIndex &index) const
{
	if (!index.isValid())
		return QModelIndex();

	CategItem *childItem = static_cast<CategItem*>(index.internalPointer());
	CategItem *parentItem = childItem->parent();

	if (parentItem == rootItem)
		return QModelIndex();

	return createIndex(parentItem->row(), 0, parentItem);
}

int CategManageModel::rowCount(const QModelIndex &parent) const
{
	CategItem *parentItem;
	if (parent.column() > 0)
		return 0;

	if (!parent.isValid())
		parentItem = rootItem;
	else
		parentItem = static_cast<CategItem*>(parent.internalPointer());

	return parentItem->childCount();
}

bool CategManageModel::insertRow(int row, const QModelIndex &)
{
	beginInsertRows(QModelIndex(), row, row);
	endInsertRows();
	return true;
}

void CategManageModel::loadCategories()
{
	QString req;
	QSqlQuery query;//(QSqlDatabase::database());
	qlonglong lastCateg = -1;

	req = "select categories.id cid, categories.nom cnom, scategories.id scid, scategories.nom scnom";
	req += " from scategories, categories ";
	req += " where scategories.categorie=categories.id ";

	req += "union select categories.id cid, categories.nom cnom, -1 scid, '' scnom";
	req += " from categories ";
	req += " where id not in (select categorie from scategories)";
	req += " order by categories.nom, scategories.nom";

	query.prepare(req);

	if(query.exec())
	{
		while(query.next())
		{
			if (lastCateg != query.value("cid").toLongLong())
			{
				rootItem->appendChild(new CategItem(query.value("cnom").toString(), query.value("cid").toLongLong(), rootItem));
				lastCateg = query.value("cid").toLongLong();
			}

			if (query.value("scid").toLongLong() != -1)
			{
				CategItem *categ = rootItem->child(rootItem->childCount()-1);
				categ->appendChild(new CategItem(query.value("scnom").toString(), query.value("scid").toLongLong(), categ));
			}
		}
	}
	else
	{
		throw QString("Erreur SQL: " + query.lastError().text() + ".\n Request:" + query.lastQuery());
	}
}

QModelIndex CategManageModel::addLine()
{
	rootItem->appendChild(new CategItem("", -1, rootItem, CategItem::LINE_INSERT));
	insertRow(rootItem->childCount()-1, QModelIndex());

	return index(rootItem->childCount()-1, 0, QModelIndex());
}

QModelIndex CategManageModel::addLine(const QModelIndex &pindex)
{
	CategItem *item = static_cast<CategItem*>(pindex.internalPointer());
	CategItem *goodItem;
	if (item != rootItem)
	{
		if (item->parent() != rootItem)
		{
			goodItem = item->parent();
		}
		else
		{
			goodItem = item;
		}
		goodItem->appendChild(new CategItem("", -1, goodItem, CategItem::LINE_INSERT));
		QModelIndex parentIndex = index(goodItem->row(), 0, QModelIndex());
		insertRow(goodItem->childCount()-1, parentIndex);

		return index(goodItem->childCount()-1, 0, parentIndex);
	}
	return QModelIndex();
}

bool CategManageModel::sqlRequets(QString table, CategItem *item)
{
	QSqlQuery q;//(QSqlDatabase::database());

	switch(item->getUpdate())
	{
	case CategItem::LINE_DELETE:
		if (item->getId() == -1)
		{
			return true;
		}
		q.prepare("delete from "+table+" where id=?");
		q.addBindValue(item->getId());
		break;
	case CategItem::LINE_UDP:
		q.prepare("update "+table+" set nom=? where id=?");
		if (item->getNom().length() == 0)
		{
			q.addBindValue(QVariant::String);
		}
		else
		{
			q.addBindValue(item->getNom());
		}
		q.addBindValue(item->getId());
		break;
	case CategItem::LINE_INSERT:
		if (table == "scategories")
		{
			q.prepare("insert into "+table+" (categorie, nom) values (?, ?)");
			q.addBindValue(item->parent()->getId());
		}
		else
		{
			q.prepare("insert into "+table+" (nom) values (?)");
		}

		if (item->getNom().length() == 0)
		{
			q.addBindValue(QVariant::String);
		}
		else
		{
			q.addBindValue(item->getNom());
		}
		break;
	default:
		return true;
		break;
	}

	if(!q.exec())
	{
		m_lastError = tr("SqLite erreur:") + q.lastError().text() + "(" + q.lastError().number() + ").\n" + tr("Requête:") + q.lastQuery();
		return false;
	}
	else
	{
		if (item->getUpdate() == CategItem::LINE_INSERT && table == "categories")
		{
			int key = q.lastInsertId().toInt();
			item->setId(key);
		}
	}
	q.clear();
	return true;
}

bool CategManageModel::saveLines(QModelIndex index)
{
	QVector<int> role;
	role.push_back(Qt::EditRole);
	//QModelIndex index = CategManageModel;
	emit dataChanged(index,index, role);
	for (int i =0; i < rootItem->childCount(); i++)
	{
		CategItem *item = rootItem->child(i);

		if (!sqlRequets("categories", item))
		{
			return false;
		}

		// avec le delete en cascade pas besoin de traiter les fils
		if (item->getUpdate() != CategItem::LINE_DELETE)
		{
			for (int j =0; j < item->childCount(); j++)
			{
				if (!sqlRequets("scategories", item->child(j)))
				{
					return false;
				}
			}
		}
	}
	SCategTables::refresh();
	return true;
}

bool CategManageModel::mustSave()
{
	for (int i =0; i < rootItem->childCount(); i++)
	{
		CategItem *item = rootItem->child(i);

		if (item->getUpdate() != CategItem::LINE_NOCHANGE)
		{
			return true;
		}

		for (int j =0; j < item->childCount(); j++)
		{
			if (item->child(j)->getUpdate() != CategItem::LINE_NOCHANGE)
			{
				return true;
			}
		}
	}
	return false;
}

void CategManageModel::delLine(const QModelIndex &item)
{
	if (item.isValid())
	{
		CategItem *categItem = static_cast<CategItem*>(item.internalPointer());
		categItem->setUpdate(CategItem::LINE_DELETE);

		QVector<int> role;// = new QVector<int>();
		role.push_back(Qt::BackgroundRole);

		if (categItem->childCount() > 0)
		{
			QModelIndex bottom = createIndex(categItem->childCount()-1, 0);
			emit dataChanged(item, bottom, role);
		}
		else
		{
			emit dataChanged(item, item, role);
		}
	}
}

QString CategManageModel::getIdList(QModelIndexList indexes)
{
	if (indexes.count() > 0)
	{
		QString ids;

		for (int i = 0; i < indexes.count(); i++)
		{
			CategItem *item = static_cast<CategItem*>(indexes.at(i).internalPointer());
			if (item->parent() != rootItem)
			{
				if (ids.length() > 0)
				{
					ids+=",";
				}
				ids+=QString::number(item->getId());
			}
		}
		return ids;
	}
	return NULL;
}

QString CategManageModel::getLastError()
{
	return m_lastError;
}

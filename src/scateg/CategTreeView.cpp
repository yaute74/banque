#include "CategTreeView.h"

CategTreeView::CategTreeView(QWidget *parent) : QTreeView(parent)
{

}

void CategTreeView::commitCloseEditor()
{
	QModelIndex index = currentIndex();
	if (index.isValid())
	{
		QWidget *editor = indexWidget(index);
		if (editor != NULL)
		{
			commitData(editor) ;
			closeEditor(editor,  QAbstractItemDelegate::NoHint);
		}
	}
}

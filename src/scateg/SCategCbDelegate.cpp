#include "SCategCbDelegate.h"
#include "SCategTables.h"
#include "QComboSCateg.h"
#include <QtSql>

SCategCbDelegate::SCategCbDelegate(QObject* parent) : QStyledItemDelegate(parent)
{
}

SCategCbDelegate::~SCategCbDelegate()
{
}

QWidget* SCategCbDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem&, const QModelIndex&) const
{
	std::map<QString, qlonglong>::const_iterator it;
	//QComboBox* cb = new QComboBox(parent);
	QComboSCateg* cb = new QComboSCateg(parent);

//	QFont font;
//	std::map<QString, qlonglong> *scateg = SCategTables::getSoucategByName();

//	font.setPointSize(font.pointSize() - 2);
//	cb->setFont(font);

//	for(it = scateg->begin(); it!=scateg->end(); ++it)
//	{
//		cb->addItem(it->first);
//	}

	return cb;
}


void SCategCbDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
	if (QComboBox* cb = qobject_cast<QComboBox*>(editor))
	{
		QString currentText = index.data(Qt::DisplayRole).toString();
		int cbIndex = cb->findText(currentText);

		if (cbIndex >= 0)
		{
			cb->setCurrentIndex(cbIndex);
			cb->showPopup();
		}
	}
}


void SCategCbDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
	if (QComboBox* cb = qobject_cast<QComboBox*>(editor))
	{
		model->setData(index, SCategTables::getSoucategByName()->at(cb->currentText()), Qt::EditRole);
	}
}

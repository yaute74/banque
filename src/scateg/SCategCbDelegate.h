#ifndef SCATEGCBDELEGATE_H
#define SCATEGCBDELEGATE_H

#include <QtWidgets>

class SCategCbDelegate : public QStyledItemDelegate
{
	Q_OBJECT
public:
	SCategCbDelegate(QObject* parent=Q_NULLPTR);
	~SCategCbDelegate();

	virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
	virtual void setEditorData(QWidget* editor, const QModelIndex& index) const;
	virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;

private:
	//void LoadSousCategories();

	//std::map<QString, qlonglong> m_scateg;
};

#endif // SCATEGCBDELEGATE_H

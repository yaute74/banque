#include "QComboSCateg.h"
#include "SCategTables.h"

QComboSCateg::QComboSCateg(QWidget *parent, bool smallFont, qlonglong selId) : QComboBox(parent)
{
	int index = loadData(selId);

	if (smallFont)
	{
		QFont font;
		font.setPointSize(font.pointSize() - 2);
		this->setFont(font);
	}

	if (index != -1)
	{
		this->setCurrentIndex(index);
	}
}

int QComboSCateg::loadData(qlonglong selId)
{
	std::map<QString, qlonglong>::const_iterator it;
	std::map<QString, qlonglong> *scateg = SCategTables::getSoucategByName();
	int index = -1;

	for(it = scateg->begin(); it!=scateg->end(); ++it)
	{
		this->addItem(it->first);
		if (selId == it->second)
		{
			index = this->count()-1;
		}
	}
	return index;
}

qlonglong QComboSCateg::getSelectId()
{
	return SCategTables::getSoucategByName()->at(this->currentText());
}

#ifndef CATEGMANAGE_H
#define CATEGMANAGE_H

#include "CategTreeView.h"
#include "CategManageModel.h"
#include <QtWidgets>

class CategManage : public QMainWindow
{
	Q_OBJECT

public:
	CategManage(QWidget *parent);

public slots:
	void addLineCateg();
	void addLineSCateg();
	bool saveLines();
	void delLine();

protected:
	 void closeEvent(QCloseEvent *event);

private:
	void createMenu();
	void createLayout();

	CategTreeView *m_treeview;
	CategManageModel *m_model;
};

#endif // CATEGMANAGE_H

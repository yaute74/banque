#include "CategManage.h"

CategManage::CategManage(QWidget *parent) : QMainWindow(parent)
{
	this->setGeometry(0, 0, 400, 500);
	this->setWindowTitle(tr("Catégories"));

	// Center frame
	QRect r = this->geometry();
	r.moveCenter(QApplication::desktop()->availableGeometry().center());
	this->setGeometry(r);

	m_treeview = new CategTreeView(this);
	createMenu();
	setCentralWidget(m_treeview);
	createLayout();
}

void CategManage::createMenu()
{
	QAction *actMvtAddCateg = new QAction(QIcon(":/images/add.png"), tr("Ajouter catégorie"), this);
	QAction *actMvtAddSCateg = new QAction(QIcon(":/images/add2.png"), tr("Ajouter sous catégorie"), this);
	QAction *actMvtSave = new QAction(QIcon(":/images/save.png"), tr("Enregistrer"), this);
	QAction *actMvtDel = new QAction(QIcon(":/images/del.png"), tr("Supprimer"), this);
	QToolBar *toolBarMain = addToolBar("Main");

	toolBarMain->addAction(actMvtAddCateg);
	toolBarMain->addAction(actMvtAddSCateg);
	toolBarMain->addAction(actMvtSave);
	toolBarMain->addAction(actMvtDel);

	connect(actMvtAddCateg, SIGNAL(triggered()), this, SLOT(addLineCateg()));
	connect(actMvtAddSCateg, SIGNAL(triggered()), this, SLOT(addLineSCateg()));
	connect(actMvtSave, SIGNAL(triggered()), this, SLOT(saveLines()));
	connect(actMvtDel, SIGNAL(triggered()), this, SLOT(delLine()));
}

void CategManage::createLayout()
{
	m_model = new CategManageModel();
	m_treeview->setModel(m_model);
	//m_treeview->expandAll();
}

void CategManage::addLineCateg()
{
	QModelIndex index = m_model->addLine();

	//QModelIndex index = m_treeview->model()->index(10, 0, QModelIndex());
	m_treeview->scrollToBottom();
	m_treeview->edit(index);
	m_treeview->setCurrentIndex(index);

	//	void QAbstractItemView::edit ( const QModelIndex & index ) [slot]
	//	void QAbstractItemView::closeEditor ( QWidget * editor, QAbstractItemDelegate::EndEditHint hint ) [virtual protected slot]
	//	void QAbstractItemView::editorDestroyed ( QObject * editor )   [virtual protected slot]
}

void CategManage::addLineSCateg()
{
	QModelIndexList indexes = m_treeview->selectionModel()->selection().indexes();
	if (indexes.count() > 0)
	{
		QModelIndex index = m_model->addLine(indexes.at(0));

		m_treeview->scrollTo(index);
		m_treeview->edit(index);
		m_treeview->setCurrentIndex(index);
	}
}

bool CategManage::saveLines()
{
	m_treeview->commitCloseEditor();

	if (m_model->saveLines(m_treeview->currentIndex()))
	{
		delete m_model;
		createLayout();
	}
	else
	{
		QMessageBox::critical(this, tr("Erreur"), m_model->getLastError());
		return false;
	}
	return true;
}

void CategManage::closeEvent(QCloseEvent *event)
{
	m_treeview->commitCloseEditor();

	if (m_model->mustSave())
	{
		int reponse = QMessageBox::question(this, tr("Modifications"), tr("Sauvegarder les modifications ?"), QMessageBox ::Yes | QMessageBox::No | QMessageBox::Cancel);

		if (reponse == QMessageBox::Yes)
		{
			if (!saveLines())
			{
				event->ignore();
				return;
			}
		}
		else if (reponse == QMessageBox::Cancel)
		{
			event->ignore();
			return;
		}
	}

	event->accept();
}

void CategManage::delLine()
{
	QModelIndexList indexes = m_treeview->selectionModel()->selection().indexes();
	if (indexes.count() > 0)
	{
		m_model->delLine(indexes.at(0));
	}
}

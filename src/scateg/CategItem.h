#ifndef CATEGITEM_H
#define CATEGITEM_H

#include <QtWidgets>

class CategItem
{
public:
	CategItem(const QString nom, qlonglong id, CategItem *parent = 0, int update = LINE_NOCHANGE);
	~CategItem();

	enum { LINE_NOCHANGE=0, LINE_UDP, LINE_INSERT, LINE_DELETE};

	void appendChild(CategItem *child);

	CategItem *child(int row);
	int childCount() const;
	int columnCount() const;
	QString getNom() const;
	int row() const;
	void setNom(QString value);
	int getUpdate();
	void setUpdate(int value);
	qlonglong getId();
	void setId(qlonglong value);
	CategItem *parent();

private:
	QList<CategItem*> childItems;
	QString m_nom;
	qlonglong m_id;
	CategItem *parentItem;
	int m_update;
};

#endif // CATEGITEM_H

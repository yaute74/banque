#include "CategItem.h"

#include <QStringList>


CategItem::CategItem(const QString nom, qlonglong id, CategItem *parent, int update) : m_nom(nom), m_id(id), parentItem(parent), m_update(update)
{

}

CategItem::~CategItem()
{
	qDeleteAll(childItems);
}

void CategItem::appendChild(CategItem *item)
{
	childItems.append(item);
}

CategItem *CategItem::child(int row)
{
	return childItems.value(row);
}

int CategItem::childCount() const
{
	return childItems.count();
}

int CategItem::columnCount() const
{
	return 1;
}

QString CategItem::getNom() const
{
	return m_nom;
}

CategItem *CategItem::parent()
{
	return parentItem;
}

int CategItem::row() const
{
	if (parentItem)
		return parentItem->childItems.indexOf(const_cast<CategItem*>(this));

	return 0;
}

void CategItem::setNom(QString value)
{
	if (m_update == LINE_NOCHANGE)
		m_update = LINE_UDP;

	m_nom = value;
}

void CategItem::setId(qlonglong value)
{
	if (m_id == -1)
		m_id = value;
}


int CategItem::getUpdate()
{
	return m_update;
}

void CategItem::setUpdate(int value)
{
	m_update = value;
	if (m_update == LINE_DELETE)
	{
		for (int i = 0; i < childItems.size(); i++)
		{
			childItems.at(i)->setUpdate(LINE_DELETE);
		}
	}
}

qlonglong CategItem::getId()
{
	return m_id;
}

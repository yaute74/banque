#ifndef CATEGMANAGEMODEL_H
#define CATEGMANAGEMODEL_H

#include "CategItem.h"
#include <QtWidgets>

class CategManageModel : public QAbstractItemModel
{

public:
	CategManageModel(QObject *parent = 0, bool readOnly = false);
	~CategManageModel();

	QVariant data(const QModelIndex &index, int role) const;
	bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
	Qt::ItemFlags flags(const QModelIndex &index) const;
	QVariant headerData(int section, Qt::Orientation orientation,
						int role = Qt::DisplayRole) const;
	QModelIndex index(int row, int column,
					  const QModelIndex &parent = QModelIndex()) const;
	QModelIndex parent(const QModelIndex &index) const;
	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;
	bool insertRow(int row, const QModelIndex &parent);
	bool sqlRequets(QString table, CategItem *item);
	QModelIndex addLine();
	QModelIndex addLine(const QModelIndex &index);
	bool saveLines(QModelIndex index);
	void delLine(const QModelIndex &item);
	QString getLastError();
	bool mustSave();
	QString getIdList(QModelIndexList indexes);

private:
	void loadCategories();

	CategItem *rootItem;
	QString m_lastError;
	bool m_readOnly;
};

#endif // CATEGMANAGEMODEL_H



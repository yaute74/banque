#include "SCategTables.h"
#include <QtSql>

SCategTables::SCategTables(/*qlonglong id, qlonglong idcateg, QString nom*/) //: m_id(id), m_idcateg(idcateg), m_nom(nom)
{
	load();
}

void SCategTables::load()
{
	QString req("select scategories.id, categories.nom cnom, scategories.nom scnom from scategories,categories where scategories.categorie=categories.id");
	QSqlQuery query(req);

	m_scateg_by_id.clear();

	if(query.exec())
	{
		while(query.next())
		{
			m_scateg_by_id[query.value("id").toLongLong()] = query.value("cnom").toString() + "->" + query.value("scnom").toString();
			m_scateg_by_name[query.value("cnom").toString() + "->" + query.value("scnom").toString()] = query.value("id").toLongLong();
		}
	}
	else
	{
		throw QString("Erreur SQL: " + query.lastError().text() + ".\n Request:" + query.lastQuery());
	}
}

std::map<qlonglong, QString> *SCategTables::getSoucategById()
{
	if (instance == NULL)
	{
		instance = new SCategTables();
	}

	return &m_scateg_by_id;
}

std::map<QString, qlonglong> *SCategTables::getSoucategByName()
{
	if (instance == NULL)
	{
		instance = new SCategTables();
	}

	return &m_scateg_by_name;
}

void SCategTables::refresh()
{
	if (instance != NULL)
	{
		delete instance;
		instance = NULL;
	}
}

std::map<qlonglong, QString> SCategTables::m_scateg_by_id;
std::map<QString, qlonglong> SCategTables::m_scateg_by_name;
SCategTables *SCategTables::instance = NULL;

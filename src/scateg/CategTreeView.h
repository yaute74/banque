#ifndef CATEGTREEVIEW_H
#define CATEGTREEVIEW_H

#include <QtWidgets>

class CategTreeView : public QTreeView
{
public:
	CategTreeView(QWidget *parent = Q_NULLPTR);
	void commitCloseEditor();
};

#endif // CATEGTREEVIEW_H

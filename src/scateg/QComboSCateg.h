#ifndef QCOMBOSCATEG_H
#define QCOMBOSCATEG_H

#include <QtWidgets>

class QComboSCateg : public QComboBox
{
public:
	QComboSCateg(QWidget *parent = Q_NULLPTR, bool smallFont = true, qlonglong selId = -1);
	qlonglong getSelectId();

private:
	int loadData(qlonglong selId);
};

#endif // QCOMBOSCATEG_H

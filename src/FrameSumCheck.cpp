#include "FrameSumCheck.h"

FrameSumCheck::FrameSumCheck(QWidget *parent) : QGroupBox(parent)
{
	createLayout();
}

void FrameSumCheck::createLayout()
{
	QGridLayout *layout = new QGridLayout();
	QLabel *lblcurCheck = new QLabel(tr("Cumul des checks:"));
	QLabel *lblcurFullCheck = new QLabel(tr("Cumul de tous les checks:"));
	QFont boldFont;

	boldFont.setBold(true);
	m_curCheck = new QLabel();
	m_curFullCheck = new QLabel();
	m_curCheck->setFont(boldFont);
	m_curFullCheck->setFont(boldFont);

	layout->addWidget(lblcurCheck, 0, 0, 1, 1);
	layout->addWidget(m_curCheck, 0, 1, 1, 1);
	layout->addWidget(lblcurFullCheck, 1, 0, 1, 1);
	layout->addWidget(m_curFullCheck, 1, 1, 1, 1);
	setLayout(layout);
}


void FrameSumCheck::setCurCheck(double value)
{
	QPalette palette;

	if (value >= 0)
	{
		palette.setColor(QPalette::WindowText, Qt::blue);
	}
	else
	{
		palette.setColor(QPalette::WindowText, Qt::red);
	}
	m_curCheck->setPalette(palette);
	m_curCheck->setText(QString::number(value));
}

void FrameSumCheck::setCurFullCheck(double value)
{
	QPalette palette;

	if (value >= 0)
	{
		palette.setColor(QPalette::WindowText, Qt::blue);
	}
	else
	{
		palette.setColor(QPalette::WindowText, Qt::red);
	}
	m_curFullCheck->setPalette(palette);
	m_curFullCheck->setText(QString::number(value));
}

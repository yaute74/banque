#ifndef FRAMEFILTER_H
#define FRAMEFILTER_H

#include "src/scateg/CategManageModel.h"
#include <QtWidgets>

class FrameFilter : public QGroupBox
{
	Q_OBJECT

public:
	FrameFilter(QWidget *parent);

	bool isFilter();
	QString getFilter();
	std::vector<QVariant> *getBindValues();
	//void refreshCateg(); TODO

public slots:
	void initWidgets();

private:
	void createLayout();

	QRadioButton *m_checkYesNo;
	QRadioButton *m_checkYes;
	QRadioButton *m_checkNo;
	QDoubleSpinBox *m_debit;
	QTreeView *m_tvCateg;
	CategManageModel *m_model;
	std::vector<QVariant> *m_bindvalues;
};

#endif // FRAMEFILTER_H

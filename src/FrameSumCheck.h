#ifndef FRAMESUMCHECK_H
#define FRAMESUMCHECK_H

#include <QtWidgets>

class FrameSumCheck : public QGroupBox
{
public:
	FrameSumCheck(QWidget *parent = Q_NULLPTR);
	void setCurCheck(double value);
	void setCurFullCheck(double value);

private:
	void createLayout();

	QLabel *m_curCheck;
	QLabel *m_curFullCheck;
};

#endif // FRAMESUMCHECK_H

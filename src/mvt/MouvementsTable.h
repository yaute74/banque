#ifndef MOUVEMENTSTABLE_H
#define MOUVEMENTSTABLE_H

#include <QtWidgets>

class MouvementsTable : public QTableView
{
public:
	MouvementsTable(QWidget *parent = Q_NULLPTR);
	void commitCloseEditor();
};

#endif // MOUVEMENTSTABLE_H


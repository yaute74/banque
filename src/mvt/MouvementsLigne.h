#ifndef MOUVEMENTSLIGNE_H
#define MOUVEMENTSLIGNE_H

#include <QtWidgets>

class MouvementsLigne
{
public:
	enum { LINE_NOCHANGE=0, LINE_UDP, LINE_INSERT, LINE_DELETE};
	enum { CHECK_NO=0, CHECK_YES, CHECK_NOT_VALIDATE };

	MouvementsLigne(qlonglong id, int datey, int datem, int dated, qlonglong idcateg, double sum, QString commentaire="", double debit=0, qlonglong prevision=0, int check=0, int updtype=LINE_NOCHANGE);
	qlonglong getId() const;
	QDate getDate() const;
	void setDate(QDate newDate);
	qlonglong getIDCateg() const;
	void setIDCateg(qlonglong id);
	QString getCommentaire() const;
	void setCommentaire(QString value);
	double getDebit() const;
	void setDebit(double value);
	bool getIsPrevision() const;
	qlonglong getPrevision() const;
	bool getIsCheck() const;
	int getCheck() const;
	bool getIsCheckNotValidate() const;
	void setIsCheck(bool value);
	void setCheckNotValidate();
	void setCheckValidate();
	int getIsUpdate();
	void setUpdate(int value);
	double getSum();
	void setSum(double value);

private:

	qlonglong m_id;
	QDate m_date;
	qlonglong m_idcateg;
	double m_sum;
	QString m_commentaire;
	double m_debit;
	qlonglong m_prevision;
	int m_check;
	bool m_check_already_validate;
	int m_update;

};

#endif // MOUVEMENTSLIGNE_H

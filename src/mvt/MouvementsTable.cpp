#include "MouvementsTable.h"

MouvementsTable::MouvementsTable(QWidget *parent) : QTableView(parent)
{

}

void MouvementsTable::commitCloseEditor()
{
	QModelIndex index = currentIndex();
	if (index.isValid())
	{
		QWidget *editor = indexWidget(index);
		if (editor != NULL)
		{
			commitData(editor) ;
			closeEditor(editor,  QAbstractItemDelegate::NoHint);
		}
	}
}

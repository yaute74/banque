#include "MouvementsLigne.h"

MouvementsLigne::MouvementsLigne(qlonglong id, int datey, int datem, int dated, qlonglong idcateg, double sum, QString commentaire, double debit, qlonglong prevision, int check, int updtype):
	m_id(id), m_date(datey, datem, dated), m_idcateg(idcateg), m_sum(sum), m_commentaire(commentaire), m_debit(debit), m_prevision(prevision), m_check(check), m_update(updtype)
{
	if (m_check == CHECK_YES)
	{
		m_check_already_validate = true;
	}
	else
	{
		m_check_already_validate = false;
	}
}

qlonglong MouvementsLigne::getId() const
{
	return m_id;
}

void MouvementsLigne::setDate(QDate newDate)
{
	if (m_date != newDate)
	{
		m_date = newDate;
		if (m_update == LINE_NOCHANGE)
		{
			m_update = LINE_UDP;
		}
	}
}

QDate MouvementsLigne::getDate() const
{
	return m_date;
}

qlonglong MouvementsLigne::getIDCateg() const
{
	return m_idcateg;
}

void MouvementsLigne::setIDCateg(qlonglong id)
{
	if (m_idcateg != id)
	{
		m_idcateg = id;
		if (m_update == LINE_NOCHANGE)
		{
			m_update = LINE_UDP;
		}
	}
}

QString MouvementsLigne::getCommentaire() const
{
	return m_commentaire;
}

void MouvementsLigne::setCommentaire(QString value)
{
	if (m_commentaire != value)
	{
		m_commentaire = value;
		if (m_update == LINE_NOCHANGE)
		{
			m_update = LINE_UDP;
		}
	}
}

double MouvementsLigne::getDebit() const
{
	return m_debit;
}

void MouvementsLigne::setDebit(double value)
{
	if (m_debit != value)
	{
		m_debit = value;
		if (m_update == LINE_NOCHANGE)
		{
			m_update = LINE_UDP;
		}
	}
}

bool MouvementsLigne::getIsPrevision() const
{
	if (m_prevision != 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

qlonglong MouvementsLigne::getPrevision() const
{
	return m_prevision;
}

bool MouvementsLigne::getIsCheck() const
{
	return m_check!=CHECK_NO;
}

int MouvementsLigne::getCheck() const
{
	return m_check;
}

bool MouvementsLigne::getIsCheckNotValidate() const
{
	return m_check==CHECK_NOT_VALIDATE;
}

void MouvementsLigne::setIsCheck(bool value)
{
	if (value == true)
	{
		if (m_check_already_validate)
		{
			m_check = CHECK_YES;
		}
		else
		{
			m_check = CHECK_NOT_VALIDATE;
		}
	}
	else
	{
		m_check = CHECK_NO;
	}
	if (m_update == LINE_NOCHANGE)
	{
		m_update = LINE_UDP;
	}
}

void MouvementsLigne::setCheckNotValidate()
{
	m_check = CHECK_NO;
	m_check_already_validate = false;
}

void MouvementsLigne::setCheckValidate()
{
	m_check = CHECK_YES;
	m_check_already_validate = true;
}

int MouvementsLigne::getIsUpdate()
{
	return m_update;
}

void MouvementsLigne::setUpdate(int value)
{
	if (value == LINE_DELETE || value == LINE_NOCHANGE || value == LINE_UDP || value == LINE_INSERT)
		m_update = value;
}

double MouvementsLigne::getSum()
{
	return m_sum+m_debit;
}

void MouvementsLigne::setSum(double value)
{
	m_sum = value;
}

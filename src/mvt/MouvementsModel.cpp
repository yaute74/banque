#include "MouvementsModel.h"
#include "src/scateg/SCategTables.h"
#include <QtSql>


MouvementsModel::MouvementsModel(QObject *parent, FrameFilter *filter, int year) : QAbstractTableModel(parent), m_filter(filter), m_year(year)
{
	m_sumCheckYes = 0;
	m_sumCheckNotValidate = 0;
	LoadMouvements();
}

int MouvementsModel::columnCount(const QModelIndex &) const
{
	return 8;
}

int MouvementsModel::rowCount(const QModelIndex &) const
{
	return m_data.size();
}

QVariant MouvementsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal)
	{
		if (role == Qt::DisplayRole)
		{
			switch (section)
			{
			case COL_ID:
				return QString(tr("Id"));
				break;
			case COL_DATE:
				return QString(tr("Date"));
				break;
			case COL_CATEG:
				return QString(tr("Catégorie"));
				break;
			case COL_COM:
				return QString(tr("Commentaire"));
				break;
			case COL_DEB:
				return QString(tr("Débit"));
				break;
			case COL_PREV:
				return QString(tr(""));
				break;
			case COL_CHK:
				return QString(tr(""));
				break;
			case COL_SUM:
				return QString("Cumul");
				break;
			}
		}
		else if (role == Qt::SizeHintRole)
		{
			QSize size(100, 20);
			switch (section)
			{
			case COL_ID:
				size.setWidth(20);
				break;
			case COL_DATE:
				size.setWidth(100);
				break;
			case COL_CATEG:
				size.setWidth(300);
				break;
			case COL_COM:
				size.setWidth(270);
				break;
			case COL_DEB:
				size.setWidth(100);
				break;
			case COL_PREV:
				size.setWidth(25);
				break;
			case COL_CHK:
				size.setWidth(25);
				break;
			case COL_SUM:
				size.setWidth(100);
				break;
			}
			return QVariant(size);
		}
	}

	return QVariant();
}

QVariant MouvementsModel::data(const QModelIndex &index, int role) const
{
	int row = index.row();
	int col = index.column();

	switch(role){
	case Qt::DisplayRole:
		switch(col)
		{
		case COL_ID:
			return  m_data[row]->getId();
			break;
		case COL_DATE:
			return m_data[row]->getDate();
			break;
		case COL_CATEG:
			return SCategTables::getSoucategById()->at(m_data[row]->getIDCateg());
			//return m_scateg.at(m_data[row]->getIDCateg());
			break;
		case COL_COM:
			return m_data[row]->getCommentaire();
			break;
		case COL_DEB:
			return m_data[row]->getDebit();
			break;
		case COL_PREV:
			break;
		case COL_CHK:
			break;
		case COL_SUM:
			return m_data[row]->getSum();
			break;
		}
		break;
	case Qt::EditRole:
		switch(col)
		{
		case COL_DATE:
			return m_data[row]->getDate();
			break;
		case COL_COM:
			return m_data[row]->getCommentaire();
			break;
		case COL_DEB:
			return m_data[row]->getDebit();
			break;
		}
		break;
	case Qt::TextColorRole:
		if (col == COL_SUM)
		{
			if(m_data[row]->getSum() < 1000 && m_data[row]->getSum() > 0)
			{
				QBrush redBackground(QColor(195,56,247));
				return redBackground;
			}

			else if(m_data[row]->getSum() < 0)
			{
				QBrush redBackground(Qt::red);
				return redBackground;
			}
		}
		else if (col == COL_CHK && m_data[row]->getIsCheckNotValidate())
		{
			QBrush redBackground(Qt::cyan);
			return redBackground;
		}
		break;
	case Qt::DecorationRole:
		if (col == COL_PREV)
		{
			if (m_data[row]->getIsPrevision())
				return QIcon(":/images/prev.png");
		}
		break;
	case Qt::BackgroundRole:

        if (col == COL_CHK && m_data[row]->getIsCheckNotValidate())
        {
            QBrush redBackground(QColor(224,247,255));
            return redBackground;
        }

		switch(m_data[row]->getIsUpdate())
		{
		case MouvementsLigne::LINE_NOCHANGE:
			if (row % 2)
			{
				return QBrush(QColor(248,248,248));
			}
			break;
		case MouvementsLigne::LINE_UDP:
		{
			return QBrush(QColor(255,244,207));
		}
			break;
		case MouvementsLigne::LINE_INSERT:
		{
			return QBrush(QColor(255,231,207));
		}
			break;
		case MouvementsLigne::LINE_DELETE:
		{
			return QBrush(QColor(205,205,205));;
		}
			break;
		}
		break;
	case Qt::TextAlignmentRole:
		if (col == COL_DEB)
		{
			if (m_data[row]->getDebit() >= 0)
			{
				return Qt::AlignLeft + Qt::AlignVCenter;
			}
			else
			{
				return Qt::AlignRight + Qt::AlignVCenter;
			}
		}
		else if (col != COL_COM && col != COL_CATEG)
		{
			return Qt::AlignHCenter + Qt::AlignVCenter;
		}
		else
		{
			return Qt::AlignLeft + Qt::AlignVCenter;
		}
		break;
	case Qt::CheckStateRole:

		if (col == COL_CHK)
		{
			if (m_data[row]->getIsCheck())
			{
				return Qt::Checked;
			}
			else
			{
				return Qt::Unchecked;
			}
		}
	}

	return QVariant();
}

void MouvementsModel::updSumCheck(int row, bool add)
{
	if (m_data[row]->getIsCheck())
	{
		if (m_data[row]->getIsCheckNotValidate())
		{
			if (add)
			{
				m_sumCheckNotValidate+=m_data[row]->getDebit();
			}
			else
			{
				m_sumCheckNotValidate-=m_data[row]->getDebit();
			}
		}
		else
		{
			if (add)
			{
				m_sumCheckYes+=m_data[row]->getDebit();
			}
			else
			{
				m_sumCheckYes-=m_data[row]->getDebit();
			}
		}
		emit changeSumCheck();
	}
}

double MouvementsModel::getSumCheck()
{
	return m_sumCheckYes;
}

double MouvementsModel::getSumFullCheck()
{
	return m_sumCheckYes+m_sumCheckNotValidate;
}

bool MouvementsModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
	int row = index.row();
	int col = index.column();

	qDebug() << value.toString();

	if (role == Qt::EditRole)
	{
		switch(col)
		{
		case COL_DATE:
			m_data[row]->setDate(value.toDate());
			break;
		case COL_CATEG:
			m_data[row]->setIDCateg(value.toLongLong());
			break;
		case COL_COM:
			m_data[row]->setCommentaire(value.toString());
			break;
		case COL_DEB:
			m_data[row]->setDebit(value.toDouble());
			recalculSum(row);
			updSumCheck(row, true);
			break;
		}
		refreshColorLine(row);
		return true;
	}
	else if (role == Qt::CheckStateRole && col == COL_CHK)
	{
		if (value == Qt::Unchecked)
		{
			updSumCheck(row, false);
			m_data[row]->setIsCheck(false);
		}
		else
		{
			m_data[row]->setIsCheck(true);
			updSumCheck(row, true);
		}
		refreshColorLine(row);
		return true;
	}
	return false;
}

Qt::ItemFlags MouvementsModel::flags(const QModelIndex & index) const
{
	Qt::ItemFlags flags = Qt::ItemIsSelectable | Qt::ItemIsEnabled;
	int col = index.column();
	int row = index.row();

	if (m_data[row]->getIsUpdate() == MouvementsLigne::LINE_DELETE || (m_filter != Q_NULLPTR && m_filter->isFilter()))
	{
		return flags;
	}

	switch(col)
	{
	case COL_DATE:
	case COL_CATEG:
	case COL_COM:
	case COL_DEB:
		flags |= Qt::ItemIsEditable;
	case COL_CHK:
		flags |= Qt::ItemIsUserCheckable;
		break;
	}

	return flags;
}

bool MouvementsModel::insertRow(int row, const QModelIndex &)
{
	beginInsertRows(QModelIndex(), row, row);
	endInsertRows();
	return true;
}


void MouvementsModel::LoadOldMouvements()
{
	QString req;
	QSqlQuery query;//(QSqlDatabase::database());

	req = "SELECT sum(debit) debit, valide";
	req += " from mouvements";
	req += " where mdate < ?";
	req += " group by valide";

	query.prepare(req);
	query.addBindValue(QString::number(m_year-1) + "-11-01 00:00:00");
	m_sumOldMvt = 0;

	qDebug() << req;
	qDebug() << QString::number(m_year-1) + "-11-01 00:00:00";

	if(query.exec())
	{
		while(query.next())
		{
			switch (query.value("valide").toLongLong())
			{
			case MouvementsLigne::CHECK_YES:
				m_sumCheckYes += query.value("debit").toDouble();
				break;
			case MouvementsLigne::CHECK_NOT_VALIDATE:
				m_sumCheckNotValidate += query.value("debit").toDouble();
				break;
			}
			m_sumOldMvt+=query.value("debit").toDouble();
		}
	}
	else
	{
		throw QString("Erreur SQL: " + query.lastError().text() + ".\n Request:" + query.lastQuery());
	}
}

void MouvementsModel::LoadMouvements()
{
	if (m_filter == Q_NULLPTR || !m_filter->isFilter())
		LoadOldMouvements();

	QString req;
	QSqlQuery query;//(QSqlDatabase::database());
	double tmpSum = m_sumOldMvt;

	req = "SELECT id, strftime('%Y', mdate) year, strftime('%m', mdate) month, strftime('%d', mdate) day, scategorie, commentaire, debit, prevision, valide";
	req += " from mouvements";
	req += " where";
	if (m_filter == Q_NULLPTR || !m_filter->isFilter())
	{
		req += " mdate >= ? and mdate < ?";
	}
	else
	{
		req += m_filter->getFilter();
	}
	req += " order by mdate";

	qDebug() << req;
	query.prepare(req);
	if (m_filter == Q_NULLPTR || !m_filter->isFilter())
	{
		query.addBindValue(QString::number(m_year-1) + "-11-01 00:00:00");
		query.addBindValue(QString::number(m_year+1) + "-03-01 00:00:00");
		qDebug() << QString::number(m_year-1) + "-11-01 00:00:00";
		qDebug() << QString::number(m_year+1) + "-03-01 00:00:00";
	}
	else
	{
		std::vector<QVariant> *filterBind = m_filter->getBindValues();
		for (size_t i = 0; i < filterBind->size(); i++)
		{
			qDebug() << filterBind->at(i);
			query.addBindValue(filterBind->at(i));
		}
	}


	if(query.exec())
	{
		while(query.next())
		{
			m_data.push_back(new MouvementsLigne(query.value("id").toLongLong(),
												 query.value("year").toInt(),
												 query.value("month").toInt(),
												 query.value("day").toInt(),
												 query.value("scategorie").toLongLong(),
												 tmpSum,
												 query.value("commentaire").toString(),
												 query.value("debit").toDouble(),
												 query.value("prevision").toInt(),
												 query.value("valide").toInt()));
			tmpSum+=query.value(6).toDouble();
			updSumCheck(m_data.size()-1, true);
		}
	}
	else
	{
		throw QString("Erreur SQL: " + query.lastError().text() + ".\n Request:" + query.lastQuery());
	}
}

void MouvementsModel::delLine(int row)
{
	m_data[row]->setUpdate(MouvementsLigne::LINE_DELETE);
	// TODO faire un uncheck pour le calcul des cumul
	refreshColorLine(row);
}

void MouvementsModel::refreshColorLine(int row)
{
	QModelIndex top = createIndex(row, 0);
	QModelIndex bottom = createIndex(row, columnCount()-1);
	QVector<int> role;// = new QVector<int>();
	role.push_back(Qt::BackgroundRole);
	emit dataChanged(top, bottom, role);
}

QModelIndex MouvementsModel::addLine(QDate date, qlonglong idprev, double debit, qlonglong scategorie)
{
	std::vector<MouvementsLigne*>::iterator it;
	int newrow = 0;

	if (scategorie == -1)
	{
		scategorie = SCategTables::getSoucategById()->begin()->first;
	}

	MouvementsLigne *newLine = new MouvementsLigne(-1, date.year(), date.month(), date.day(), scategorie, 0, "", debit, idprev, 0, MouvementsLigne::LINE_INSERT);

	for (it = m_data.begin(); it != m_data.end(); it++)
	{
		if ((*it)->getDate() > date)
		{
			m_data.insert(it, newLine);
			break;
		}
		newrow++;
	}

	if (m_data.size() == 0 || it == m_data.end())
	{
		m_data.push_back(newLine);
		newrow = rowCount()-1;
	}

	insertRow(newrow);
	return index(newrow, MouvementsModel::COL_CATEG);
}

std::set<qlonglong> MouvementsModel::getPrevInCache()
{
	std::vector<MouvementsLigne*>::iterator it;
	std::set<qlonglong> list;

	for (it = m_data.begin(); it != m_data.end(); ++it)
	{
		if ((*it)->getId() == -1 && (*it)->getIsPrevision())
		{
			list.insert((*it)->getPrevision());
		}
	}
	return list;
}

bool MouvementsModel::saveLines()
{
	std::vector<MouvementsLigne*>::iterator it;
	QSqlQuery q;//(QSqlDatabase::database());

	for (it = m_data.begin(); it != m_data.end(); ++it)
	{
		switch((*it)->getIsUpdate())
		{
		case MouvementsLigne::LINE_DELETE:
			q.prepare("delete from mouvements where id=?");
			q.addBindValue((*it)->getId());
			break;
		case MouvementsLigne::LINE_UDP:
			q.prepare("update mouvements set mdate=?, scategorie=?, commentaire=?, debit=?, prevision=?, valide=? where id=?");
			q.addBindValue((*it)->getDate());
			q.addBindValue((*it)->getIDCateg());
			q.addBindValue((*it)->getCommentaire());
			q.addBindValue((*it)->getDebit());
			if ((*it)->getPrevision() == 0)
			{
				q.addBindValue(QVariant::LongLong);
			}
			else
			{
				q.addBindValue((*it)->getPrevision());
			}
			q.addBindValue((*it)->getCheck());
			q.addBindValue((*it)->getId());
			break;
		case MouvementsLigne::LINE_INSERT:
			q.prepare("insert into mouvements (mdate, scategorie, commentaire, debit, prevision, valide) values (?, ?, ?, ?, ?, ?)");
			q.addBindValue((*it)->getDate());
			q.addBindValue((*it)->getIDCateg());
			q.addBindValue((*it)->getCommentaire());
			q.addBindValue((*it)->getDebit());
			if ((*it)->getPrevision() == 0)
			{
				q.addBindValue(QVariant::LongLong);
			}
			else
			{
				q.addBindValue((*it)->getPrevision());
			}
			q.addBindValue((*it)->getCheck());
			break;
		default:
			continue;
			break;
		}
		if(!q.exec())
		{
			m_lastError = tr("SqLite erreur:") + q.lastError().text() + "(" + q.lastError().number() + ").\n" + tr("Requête:") + q.lastQuery() + "\n" +\
					QString::number((*it)->getId()) + "," + (*it)->getDate().toString() + "," + QString::number((*it)->getIDCateg()) + "," + (*it)->getCommentaire()  + "," + QString::number((*it)->getDebit())  + ","\
					+ QString::number((*it)->getPrevision()) + "," + QString::number((*it)->getCheck());
			qDebug() << m_lastError;
			//QMessageBox::critical(this->parent, tr("Database request"), "SqLite erreur:" + q.lastError().text() + "(" + q.lastError().number() + "). \nRequête:" + q.lastQuery());
			//qDebug() << q.lastError();
			return false;
		}
		q.clear();
	}
	return true;
}

bool MouvementsModel::mustSave()
{
	for (size_t i = 0; i < m_data.size(); i++)
	{
		if (m_data[i]->getIsUpdate() != MouvementsLigne::LINE_NOCHANGE)
		{
			return true;
		}
	}
	return false;
}

QString MouvementsModel::getLastError()
{
	return m_lastError;
}

void MouvementsModel::updCheck(bool validate)
{
	QVector<int> role;
	role.push_back(Qt::BackgroundRole);
	role.push_back(Qt::TextColorRole);

	for (size_t i=0; i < m_data.size(); i++)
	{
		if (m_data[i]->getIsCheckNotValidate())
		{

			if (validate)
			{
				updSumCheck(i, false);
				m_data[i]->setCheckValidate();
				updSumCheck(i, true);
			}
			else
			{
				updSumCheck(i, false);
				m_data[i]->setCheckNotValidate();
			}

			if (m_data[i]->getIsUpdate() == MouvementsLigne::LINE_NOCHANGE)
			{
				m_data[i]->setUpdate(MouvementsLigne::LINE_UDP);
			}

			QModelIndex top = createIndex(i, 0);
			QModelIndex bottom = createIndex(i, columnCount()-1);
			emit dataChanged(top, bottom, role);
		}
	}
}

void MouvementsModel::recalculSum(int sinceRow)
{
	QVector<int> role;
	role.push_back(Qt::DisplayRole);

	for (size_t i=sinceRow; i < m_data.size(); i++)
	{
		if (sinceRow == 0)
		{
			m_data[0]->setSum(m_sumOldMvt);
		}
		else
		{
			m_data[i]->setSum(m_data[i-1]->getSum());
		}
	}

	QModelIndex top = createIndex(sinceRow, COL_SUM);
	QModelIndex bottom = createIndex(rowCount()-1, COL_SUM);
	emit dataChanged(top, bottom, role);
}

MouvementsModel::~MouvementsModel()
{
	for (size_t i = 0; i < m_data.size(); i++)
	{
		delete m_data[i];
	}
}


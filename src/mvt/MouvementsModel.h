#ifndef MOUVEMENTSMODEL_H
#define MOUVEMENTSMODEL_H

#include "MouvementsLigne.h"
#include "src/FrameFilter.h"
#include <QtWidgets>
#include <set>

class MouvementsModel : public QAbstractTableModel
{

	Q_OBJECT

public:
	MouvementsModel(QObject *parent = Q_NULLPTR, FrameFilter *filter = Q_NULLPTR, int year = -1);
	int rowCount(const QModelIndex &parent = QModelIndex()) const ;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const;
	bool setData(const QModelIndex & index, const QVariant & value, int role);
	bool insertRow(int row, const QModelIndex &parent = QModelIndex());
	Qt::ItemFlags flags(const QModelIndex &) const;
	void delLine(int row);
	QModelIndex addLine(QDate date, qlonglong idprev = 0, double debit = 0, qlonglong scategorie = -1);
	bool saveLines();
	bool mustSave();
	std::set<qlonglong> getPrevInCache();
	void updCheck(bool validate);
	double getSumCheck();
	double getSumFullCheck();
	void recalculSum(int sinceRow);
	QString getLastError();
	~MouvementsModel();

	enum { COL_ID=0, COL_DATE, COL_PREV, COL_CATEG, COL_COM, COL_DEB, COL_CHK, COL_SUM};

signals:
	void changeSumCheck();

private:
	void CreateColumnHeader();
	void LoadMouvements();
	void LoadOldMouvements();
	//void LoadSousCategories();
	void refreshColorLine(int row);
	void updSumCheck(int row, bool add);

	//std::map<qlonglong, QString> m_scateg;
	std::vector<MouvementsLigne*> m_data;
	double	m_sumCheckYes;
	double	m_sumCheckNotValidate;
	double  m_sumOldMvt;
	FrameFilter *m_filter;
	QString m_lastError;
	int m_year;
};

#endif // MOUVEMENTSMODEL_H

#include "Histographe.h"
#include <QtSql>

Histographe::Histographe(RepartitionCateg *test, QWidget *parent) : m_data(test), QDialog(parent)
{
	this->setGeometry(0, 0, 1000, 600);
	this->setWindowTitle(m_data->getTitle());

	// Center frame
	QRect r = this->geometry();
	r.moveCenter(QApplication::desktop()->availableGeometry().center());
	this->setGeometry(r);

	createLayout();
}


void Histographe::createLayout()
{
	QGridLayout *gridLayout = new QGridLayout();

	qp = new QCustomPlot(this);
	horizontalScrollBar = new QScrollBar(this);
	verticalScrollBar = new QScrollBar(this);
	horizontalScrollBar->setOrientation(Qt::Horizontal);
	verticalScrollBar->setOrientation(Qt::Vertical);

	setupPlot();

	horizontalScrollBar->setRange(-500, 500);
	verticalScrollBar->setRange(-500, 500);

	// create connection between axes and scroll bars:
	connect(horizontalScrollBar, SIGNAL(valueChanged(int)), this, SLOT(horzScrollBarChanged(int)));
	connect(verticalScrollBar, SIGNAL(valueChanged(int)), this, SLOT(vertScrollBarChanged(int)));
	connect(qp->xAxis, SIGNAL(rangeChanged(QCPRange)), this, SLOT(xAxisChanged(QCPRange)));
	connect(qp->yAxis, SIGNAL(rangeChanged(QCPRange)), this, SLOT(yAxisChanged(QCPRange)));

	gridLayout->addWidget(qp, 0,0,1,1);
	gridLayout->addWidget(horizontalScrollBar, 1,0,1,2);
	gridLayout->addWidget(verticalScrollBar, 0,1,1,1);
	gridLayout->addWidget(qp, 0,0,1,1);
	setLayout(gridLayout);
}

void Histographe::setupPlot()
{
	// set dark background gradient:
	QLinearGradient gradient(0, 0, 0, 400);
	gradient.setColorAt(0, QColor(90, 90, 90));
	gradient.setColorAt(0.38, QColor(105, 105, 105));
	gradient.setColorAt(1, QColor(70, 70, 70));
	qp->setBackground(QBrush(gradient));

	QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
	int xrange = 0;

	for (int i = 0; i < m_data->getNbGroup(); i++)
	{
		QCPBars *regen = new QCPBars(qp->xAxis, qp->yAxis);
		regen->setAntialiased(false); // gives more crisp, pixel aligned bar borders
		regen->setStackingGap(1);
		regen->setName(m_data->getLbl(i));
		regen->setPen(QPen(QColor(0, 0, 0)));
		regen->setBrush(m_data->getColor(i));

		regen->setData(m_data->getTicks(i), m_data->getData(i));
		textTicker->addTicks(m_data->getTicks(i), m_data->getLblX(i));
		xrange+=m_data->getTicks(i).size();
	}

	qp->xAxis->setTicker(textTicker);
	qp->xAxis->setTickLabelRotation(60);
	qp->xAxis->setSubTicks(false);
	qp->xAxis->setTickLength(0, 4);
	qp->xAxis->setRange(0, xrange+1);
	qp->xAxis->setBasePen(QPen(Qt::white));
	qp->xAxis->setTickPen(QPen(Qt::white));
	qp->xAxis->grid()->setVisible(true);
	qp->xAxis->grid()->setPen(QPen(QColor(130, 130, 130), 0, Qt::DotLine));
	qp->xAxis->setTickLabelColor(Qt::white);
	qp->xAxis->setLabelColor(Qt::white);

	// prepare y axis:
    qp->yAxis->setRange(0, m_data->getMaxValue());
	qp->yAxis->setPadding(5); // a bit more space to the left border
	qp->yAxis->setLabel(m_data->getLblY());
	qp->yAxis->setBasePen(QPen(Qt::white));
	qp->yAxis->setTickPen(QPen(Qt::white));
	qp->yAxis->setSubTickPen(QPen(Qt::white));
	qp->yAxis->grid()->setSubGridVisible(true);
	qp->yAxis->setTickLabelColor(Qt::white);
	qp->yAxis->setLabelColor(Qt::white);
	qp->yAxis->grid()->setPen(QPen(QColor(130, 130, 130), 0, Qt::SolidLine));
	qp->yAxis->grid()->setSubGridPen(QPen(QColor(130, 130, 130), 0, Qt::DotLine));

	// setup legend:
	qp->legend->setVisible(true);
	qp->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignTop|Qt::AlignHCenter);
	qp->legend->setBrush(QColor(255, 255, 255, 100));
	qp->legend->setBorderPen(Qt::NoPen);
	QFont legendFont = font();
	legendFont.setPointSize(10);
	qp->legend->setFont(legendFont);
	qp->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
}

void Histographe::horzScrollBarChanged(int value)
{
  if (qAbs(qp->xAxis->range().center()-value/100.0) > 0.01) // if user is dragging plot, we don't want to replot twice
  {
	qp->xAxis->setRange(value/100.0, qp->xAxis->range().size(), Qt::AlignCenter);
	qp->replot();
  }
}

void Histographe::vertScrollBarChanged(int value)
{
  if (qAbs(qp->yAxis->range().center()+value/100.0) > 0.01) // if user is dragging plot, we don't want to replot twice
  {
	qp->yAxis->setRange(-value/100.0, qp->yAxis->range().size(), Qt::AlignCenter);
	qp->replot();
  }
}

void Histographe::xAxisChanged(QCPRange range)
{
  horizontalScrollBar->setValue(qRound(range.center()*100.0)); // adjust position of scroll bar slider
  horizontalScrollBar->setPageStep(qRound(range.size()*100.0)); // adjust size of scroll bar slider
}

void Histographe::yAxisChanged(QCPRange range)
{
  verticalScrollBar->setValue(qRound(-range.center()*100.0)); // adjust position of scroll bar slider
  verticalScrollBar->setPageStep(qRound(range.size()*100.0)); // adjust size of scroll bar slider
}


/*
 *  Courbe
void TestGraphic::setupPlot()
{
	// initialize axis range (and scroll bar positions via signals we just connected):
	//qp->xAxis->setRange(0, 6, Qt::AlignCenter);
	//qp->yAxis->setRange(0, 10, Qt::AlignCenter);

	// The following plot setup is mostly taken from the plot demos:
	qp->addGraph();
	qp->graph()->setPen(QPen(Qt::blue));
	qp->graph()->setBrush(QBrush(QColor(0, 0, 255, 20)));
	qp->addGraph();
	qp->graph()->setPen(QPen(Qt::red));
	QVector<double> x(500), y0(500), y1(500);
	for (int i=0; i<500; ++i)
	{
		x[i] = (i/499.0-0.5)*10;
		y0[i] = qExp(-x[i]*x[i]*0.25)*qSin(x[i]*5)*5;
		y1[i] = qExp(-x[i]*x[i]*0.25)*5;
	}
	qp->graph(0)->setData(x, y0);
	qp->graph(1)->setData(x, y1);
	qp->axisRect()->setupFullAxesBox(true);
	qp->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
}
*/

// histogramme: cumule
//void TestGraphic::setupPlot()
//{
//	// http://qcustomplot.com/index.php/demos/barchartdemo
//	// http://www.qcustomplot.com/index.php/tutorials/basicplotting

//	// set dark background gradient:
//	QLinearGradient gradient(0, 0, 0, 400);
//	gradient.setColorAt(0, QColor(90, 90, 90));
//	gradient.setColorAt(0.38, QColor(105, 105, 105));
//	gradient.setColorAt(1, QColor(70, 70, 70));
//	qp->setBackground(QBrush(gradient));

//	// create empty bar chart objects:
//	QCPBars *regen = new QCPBars(qp->xAxis, qp->yAxis);
//	QCPBars *nuclear = new QCPBars(qp->xAxis, qp->yAxis);
//	QCPBars *fossil = new QCPBars(qp->xAxis, qp->yAxis);
//	regen->setAntialiased(false); // gives more crisp, pixel aligned bar borders
//	nuclear->setAntialiased(false);
//	fossil->setAntialiased(false);
//	regen->setStackingGap(1);
//	nuclear->setStackingGap(1);
//	fossil->setStackingGap(1);
//	// set names and colors:
//	fossil->setName("Fossil fuels");
//	fossil->setPen(QPen(QColor(111, 9, 176).lighter(170)));
//	fossil->setBrush(QColor(111, 9, 176));
//	nuclear->setName("Nuclear");
//	nuclear->setPen(QPen(QColor(250, 170, 20).lighter(150)));
//	nuclear->setBrush(QColor(250, 170, 20));
//	regen->setName("Regenerative");
//	regen->setPen(QPen(QColor(0, 168, 140).lighter(130)));
//	regen->setBrush(QColor(0, 168, 140));
//	// stack bars on top of each other:
//	nuclear->moveAbove(fossil);
//	regen->moveAbove(nuclear);

//	// prepare x axis with country labels:
//	QVector<double> ticks;
//	QVector<QString> labels;
//	ticks << 1 << 2 << 3 << 4 << 5 << 6 << 7;
//	labels << "USA" << "Japan" << "Germany" << "France" << "UK" << "Italy" << "Canada";
//	QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
//	textTicker->addTicks(ticks, labels);
//	qp->xAxis->setTicker(textTicker);
//	qp->xAxis->setTickLabelRotation(60);
//	qp->xAxis->setSubTicks(false);
//	qp->xAxis->setTickLength(0, 4);
//	qp->xAxis->setRange(0, 8);
//	qp->xAxis->setBasePen(QPen(Qt::white));
//	qp->xAxis->setTickPen(QPen(Qt::white));
//	qp->xAxis->grid()->setVisible(true);
//	qp->xAxis->grid()->setPen(QPen(QColor(130, 130, 130), 0, Qt::DotLine));
//	qp->xAxis->setTickLabelColor(Qt::white);
//	qp->xAxis->setLabelColor(Qt::white);

//	// prepare y axis:
//	qp->yAxis->setRange(0, 12.1);
//	qp->yAxis->setPadding(5); // a bit more space to the left border
//	qp->yAxis->setLabel("Power Consumption in\nKilowatts per Capita (2007)");
//	qp->yAxis->setBasePen(QPen(Qt::white));
//	qp->yAxis->setTickPen(QPen(Qt::white));
//	qp->yAxis->setSubTickPen(QPen(Qt::white));
//	qp->yAxis->grid()->setSubGridVisible(true);
//	qp->yAxis->setTickLabelColor(Qt::white);
//	qp->yAxis->setLabelColor(Qt::white);
//	qp->yAxis->grid()->setPen(QPen(QColor(130, 130, 130), 0, Qt::SolidLine));
//	qp->yAxis->grid()->setSubGridPen(QPen(QColor(130, 130, 130), 0, Qt::DotLine));

//	// Add data:
//	QVector<double> fossilData, nuclearData, regenData;
//	fossilData  << 0.86*10.5 << 0.83*5.5 << 0.84*5.5 << 0.52*5.8 << 0.89*5.2 << 0.90*4.2 << 0.67*11.2;
//	nuclearData << 0.08*10.5 << 0.12*5.5 << 0.12*5.5 << 0.40*5.8 << 0.09*5.2 << 0.00*4.2 << 0.07*11.2;
//	regenData   << 0.06*10.5 << 0.05*5.5 << 0.04*5.5 << 0.06*5.8 << 0.02*5.2 << 0.07*4.2 << 0.25*11.2;
//	fossil->setData(ticks, fossilData);
//	nuclear->setData(ticks, nuclearData);
//	regen->setData(ticks, regenData);

//	// setup legend:
//	qp->legend->setVisible(true);
//	qp->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignTop|Qt::AlignHCenter);
//	qp->legend->setBrush(QColor(255, 255, 255, 100));
//	qp->legend->setBorderPen(Qt::NoPen);
//	QFont legendFont = font();
//	legendFont.setPointSize(10);
//	qp->legend->setFont(legendFont);
//	qp->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
//}

#ifndef REPARTITIONCATEG_H
#define REPARTITIONCATEG_H

#include <QtWidgets>
#include <QMainWindow>
#include "qcustomplot.h"

class RepartitionCateg
{

public:
	RepartitionCateg();
	int getNbGroup();
	QVector<double> getTicks(int);
	QVector<QString> getLblX(int);
	QVector<double> getData(int);
	double getMaxValue();
	QString getLbl(int);
	QColor getColor(int);
	QString getTitle();
	QString getLblY();

private:
	void LoadData();

	QVector<double> m_tickspos;
	QVector<QString> m_lblxpos;
	QVector<double> m_datapos;
	QVector<double> m_ticksneg;
	QVector<QString> m_lblxneg;
	QVector<double> m_dataneg;
	double m_maxvalue;
	QString m_lblpos;
	QString m_lblneg;
	QColor m_colpos;
	QColor m_colneg;
};

#endif // REPARTITIONCATEG_H

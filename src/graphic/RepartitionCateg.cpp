#include "RepartitionCateg.h"
#include <QtSql>

RepartitionCateg::RepartitionCateg()
{
	LoadData();
}

void RepartitionCateg::LoadData()
{
	QSqlQuery query;//(QSqlDatabase::database());
	QString req;
	int count = 1;

	req = "SELECT categories.nom nom, sum(debit) debit";
	req += " from mouvements, categories, scategories";
	req += " where mouvements.scategorie=scategories.id and";
	req += " scategories.categorie=categories.id";
	req += " group by categories.nom";

	qDebug() << req;
	query.prepare(req);
	//query.addBindValue(QString::number(m_year+1) + "-03-01 00:00:00");

	if(query.exec())
	{
		while(query.next())
		{
			if (query.value("debit").toDouble() > 0)
			{
				m_tickspos << count;
				m_lblxpos << query.value("nom").toString();
				m_datapos << query.value("debit").toDouble();
			}
			else
			{
				m_ticksneg << count;
				m_lblxneg << query.value("nom").toString();
				m_dataneg << -query.value("debit").toDouble();
			}
			if (qAbs(query.value("debit").toDouble()) > m_maxvalue)
			{
				m_maxvalue = qAbs(query.value("debit").toDouble());
			}

			count++;
		}
	}

	//QChar maxArrondi = QChar::Number_DecimalDigit String::number(max);
	QString maxArrondi = QString::number(qRound(m_maxvalue));
    //int first = maxArrondi.at(0).digitValue()+1;
    //if (first == 10)
        //m_maxvalue = qPow(10, maxArrondi.length());
    //else
        //m_maxvalue = first*qPow(10, maxArrondi.length());

    m_maxvalue = qPow(10, maxArrondi.length());
    //qDebug() << "GRAPHIQUE";
    //qDebug() << first;
    //qDebug() << maxArrondi.length();
    //qDebug() << qPow(10, maxArrondi.length());
}


int RepartitionCateg::getNbGroup()
{
	return 2;
}

QVector<double> RepartitionCateg::getTicks(int i)
{
	if (i==0)
	{
		return m_tickspos;
	}
	else
	{
		return m_ticksneg;
	}
}

QVector<QString> RepartitionCateg::getLblX(int i)
{
	if (i==0)
	{
		return m_lblxpos;
	}
	else
	{
		return m_lblxneg;
	}
}

QVector<double> RepartitionCateg::getData(int i)
{
	if (i==0)
	{
		return m_datapos;
	}
	else
	{
		return m_dataneg;
	}
}

double RepartitionCateg::getMaxValue()
{
	return m_maxvalue;
}

QString RepartitionCateg::getLbl(int i)
{
	if (i==0)
	{
		return QObject::tr("Positif");
	}
	else
	{
		return QObject::tr("Négatif");
	}
}

QColor RepartitionCateg::getColor(int i)
{
	if (i==0)
	{
		return QColor(24, 42, 121);
	}
	else
	{
		return QColor(220, 53, 53);
	}
}

QString RepartitionCateg::getTitle()
{
	return QObject::tr("Répartition des sommes");
}

QString RepartitionCateg::getLblY()
{
	return QObject::tr("Somme en €");
}

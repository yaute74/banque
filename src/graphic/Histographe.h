#ifndef HISTOGRAPHE_H
#define HISTOGRAPHE_H

#include <QtWidgets>
#include <QMainWindow>
#include "RepartitionCateg.h"
#include "qcustomplot.h"

class Histographe : public QDialog
{
	Q_OBJECT

public:
	Histographe(RepartitionCateg *test, QWidget *parent = Q_NULLPTR);

public slots:
	void horzScrollBarChanged(int value);
	void vertScrollBarChanged(int value);
	void xAxisChanged(QCPRange range);
	void yAxisChanged(QCPRange range);

private:
	void createLayout();
	void setupPlot();

	QScrollBar *horizontalScrollBar;
	QScrollBar *verticalScrollBar;
	QCustomPlot *qp;
	RepartitionCateg *m_data;
};

#endif // HISTOGRAPHE_H

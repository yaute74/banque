#!/bin/bash
set -e
set -x
##############################################
#
# apt install lintian
#
# git tag -a v1.0.0 -m "Init version"
##############################################

SRC_BIN=../../build-release/banque
DST_BIN=banque/usr/bin/
CTRL_FILE=banque/DEBIAN/control
#CHANGELOG=banque/usr/share/doc/banque/changelog.Debian
CHANGELOG=banque/usr/share/doc/banque/changelog
VERSION=`grep Version $CTRL_FILE | cut -d' ' -f2`
ARCH=`grep Architecture $CTRL_FILE | cut -d' ' -f2`
PROJECT=`grep Package $CTRL_FILE | cut -d' ' -f2`
DEB_FILE=${PROJECT}_${VERSION}_${ARCH}.deb
MAN_FILE=man/banque.1
DST_MAN=banque/usr/share/man/man1

CPT=`ls banque*.deb | wc -l`
if [ "$CPT" != "0" ]
then
	rm banque*.deb
fi

if [ ! -f  $SRC_BIN ]
then
	echo "file $SRC_BIN doesn't exist"
fi

if [ ! -d $DST_MAN ]
then
	mkdir -p $DST_MAN
fi

cp $MAN_FILE $DST_MAN/
gzip -9 -n $DST_MAN/banque.1

if [ ! -d $DST_BIN ]
then
	mkdir $DST_BIN
fi

if [ -f $CHANGELOG.gz ]
then
	rm $CHANGELOG.gz
fi
cp $SRC_BIN $DST_BIN
cp debian/changelog $CHANGELOG
gzip -9 -n $CHANGELOG

sudo chown root:root -R banque/*
dpkg-deb --build banque
RET=$?
sudo chown pdegerine:pdegerine -R banque/*

if [ "$RET" != "0" ]
then
	exit 1
fi

mv banque.deb $DEB_FILE
lintian $DEB_FILE



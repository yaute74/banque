#!/usr/bin/env bash
#set -x
set -e

if [ -f debian/changelog ]
then
	rm debian/changelog
fi 

# TYPE= argument to the script where 0 = MAJOR, 1 = MINOR, 2 = BUILD. Default to BUILD.
GIT_VERSION=$(git describe --tags)
CURRENT_VERSION=$(echo ${GIT_VERSION:1} | cut -d'-' -f1)
TYPE=${1:-2}

function increment_version() {
	local VERSION="$1"
	local PLACE="$2"

	IFS='.' read -r -a a <<<"$VERSION"
	((a[PLACE]++))
	echo "${a[0]}.${a[1]}.${a[2]}"
}

NEW_VERSION=$(increment_version $CURRENT_VERSION $TYPE)

#dch --create -v $NEW_VERSION && git commit -p -m "Bump version $NEW_VERSION" && git tag v${NEW_VERSION}
dch --create -v $NEW_VERSION


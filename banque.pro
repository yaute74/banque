QT       += core gui
QT       += sql

QT += widgets printsupport


#QMAKE_LFLAGS += -static

QMAKE_CXXFLAGS_WARN_ON += -Wno-reorder

SOURCES += \
    src/mvt/MouvementsLigne.cpp \
    src/mvt/MouvementsModel.cpp \
    src/main.cpp \
    src/MainUI.cpp \
    src/tools/QBoxCalendar.cpp \
    src/FrameSumCheck.cpp \
    src/FrameFilter.cpp \
    src/prev/manage/PrevManage.cpp \
    src/prev/manage/PrevModel.cpp \
    src/prev/manage/PrevObj.cpp \
    src/prev/add/PrevAddObj.cpp \
    src/prev/add/PrevAdd.cpp \
    src/scateg/SCategCbDelegate.cpp \
    src/scateg/SCategTables.cpp \
    src/scateg/CategManage.cpp \
    src/scateg/CategManageModel.cpp \
    src/scateg/CategItem.cpp \
    src/scateg/CategTreeView.cpp \
    src/prev/manage/PrevTable.cpp \
    src/mvt/MouvementsTable.cpp \
    src/autochk/QDialogReleve.cpp \
    src/autochk/ChoiceMvt.cpp \
    src/scateg/QComboSCateg.cpp \
    src/graphic/qcustomplot.cpp \
    src/graphic/Histographe.cpp \
    src/graphic/RepartitionCateg.cpp \
    src/autochk/QDialogNotSave.cpp
HEADERS += \
    src/mvt/MouvementsLigne.h \
    src/mvt/MouvementsModel.h \
    src/MainUI.h \
    src/tools/QBoxCalendar.h \
    src/FrameSumCheck.h \
    src/FrameFilter.h \
    src/prev/manage/PrevManage.h \
    src/prev/manage/PrevModel.h \
    src/prev/manage/PrevObj.h \
    src/prev/add/PrevAddObj.h \
    src/prev/add/PrevAdd.h \
    src/scateg/SCategCbDelegate.h \
    src/scateg/SCategTables.h \
    src/scateg/CategManage.h \
    src/scateg/CategManageModel.h \
    src/scateg/CategItem.h \
    src/scateg/CategTreeView.h \
    src/prev/manage/PrevTable.h \
    src/mvt/MouvementsTable.h \
    src/autochk/QDialogReleve.h \
    src/autochk/ChoiceMvt.h \
    src/scateg/QComboSCateg.h \
    src/graphic/qcustomplot.h \
    src/graphic/Histographe.h \
    src/graphic/RepartitionCateg.h \
    src/autochk/QDialogNotSave.h

RESOURCES += \
    resources.qrc

DISTFILES +=
